var init=function(testo){
  var loaderImage = Ti.UI.createImageView({
  width:54,
  height:54
});

var label=Ti.UI.createLabel({
  color:"black",
  text:testo,
  font:{
    fontSize:20,
  },
  top:"100px"
});

var view=Ti.UI.createView({
  color:"#b3b3b3",
  opacity:0.5

});

view.add(label);
view.add(loaderImage);

var loaderArrayLength=8;
var loaderIndex=1;
}
