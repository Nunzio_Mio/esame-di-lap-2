var args = $.args;
var data=[];
var a1=[];
var info=[];


var pRequest=require("parseCall");

function boh(){
  Ti.App.fireEvent("raimondo");
  $.win.close();
}

var addBookF=function(title,book,user){
  pRequest.addParseBookWhitCheck(title,book,user);

}


var serc= function(e){
  $.txt.blur();
  $.wait.getView().height=Ti.UI.FILL;
  $.wait.getView().width=Ti.UI.FILL;
  $.wait.getView().show();
  var titleSearch=$.txt.value.replace(" ","+");
  var search="https://www.googleapis.com/books/v1/volumes?q="+ titleSearch+"&orderBy=relevance";
  var web=Titanium.Network.createHTTPClient();
  web.setRequestHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
  web.open("GET",search);

  web.onload=function(){
    Ti.API.info("data json: "+this.responseText);
    data=JSON.parse(this.responseText);
    //if
    if(data["totalItems"]>0){
    for (var i = 0; i < 10; i++) {

      try{
      var cover=data["items"][i]["volumeInfo"]["imageLinks"].smallThumbnail;
    }catch(e){
      var cover="/images/notfound.png";
    }
      try{
      var titolo=data["items"][i]["volumeInfo"].title;
    }catch(e){
      var titolo="no Title";
    }

    if(typeof data["items"][i]["volumeInfo"].authors =='object'){
    var autore=data["items"][i]["volumeInfo"].authors[0];
  }else{
    var autore=data["items"][i]["volumeInfo"].authors;
  }
      try{
      var desc=data["items"][i]["volumeInfo"].description;
     }catch(e){
       var desc="Non Disponibile"
     }

      try{
      var isbn13=data["items"][i]["volumeInfo"].industryIdentifiers[0].identifier;
    }catch(e){
      var isbn13=-1;
    }

    try{
      var isbn10=data["items"][i]["volumeInfo"].industryIdentifiers[1].identifier;
    }catch(e){
      var isbn10=-1;
    }
      var row=Ti.UI.createTableViewRow();
      info[i]={
      titolo:titolo,
      autore:autore,
      copertina:cover,
      descrizione:desc,
      isbn13:isbn13,
      isbn10:isbn10
    };


    var v1=Ti.UI.createView();
      v1.borderColor="#e6e6e6";

      if(titolo.length>24){
        var t=titolo.slice(0,23);
        var newtit=t+"...";
      }
      else{
        var newtit=titolo;
      }



      var t2=Ti.UI.createLabel({
        text: autore,
        color: "black",
        left: "120px",
        top:30,
      });
      var t1=Ti.UI.createLabel({
        text: newtit,
        color: "black",
        left: "120px",
        top:10,
      });
      var c1=Ti.UI.createImageView({
        height:"150px",
        width:"100px",
        image:cover,
        left:5,
      });



      var plus=Ti.UI.createImageView({
        image:"/images/plus.png",
        right:7,
        height:"90px",
        width:"90px",

      });

//controllo del libro in possesso






      v1.add(plus);
      v1.add(t1);
      v1.add(t2);
      v1.add(c1);
      row.add(v1);
      plus.idrow=i;
      //a1.push(row);
      plus.addEventListener("click",function(e){
        var book={
          title:info[e.source.idrow].titolo,
          authors:info[e.source.idrow].autore,
          isbn:info[e.source.idrow].isbn10,
          isbn13:info[e.source.idrow].isbn13,
          desc:info[e.source.idrow].descrizione,
          imageName:info[e.source.idrow].copertina
        }
        pRequest.newFuncAdd(info[e.source.idrow].titolo,book,Parse.User.current());
        e.source.image="/images/check.png";

      });
      a1[i]=row;
  }
//fine if
}else{
  var failLabel=Ti.UI.createLabel({
    color:"black",
    text:"non ho trovato nulla",
  });
  $.books.add(failLabel);
}
$.books.setData(a1);
$.wait.getView().hide();
$.wait.getView().height=0;
$.wait.getView().width=0;
  }
  web.error=function(){
    Ti.API.info("Mi dispiace ma non troviamo il tuo libro :(");
    alert("Mi dispiace ma non troviamo il tuo libro :(");
  }
  web.send();
}



  $.books.addEventListener("longclick",function(e){
      Ti.API.info(e.index);
      Ti.API.info(info[e.index].titolo);
      Ti.API.info(info[e.index].autore);
      Ti.API.info(info[e.index].copertina);
      Ti.API.info(info[e.index].descrizione);
      var c = Alloy.createController('bookpage');
      c.gettitolo(info[e.index].titolo);
      c.getscrittore(info[e.index].autore);
      c.getlibro(info[e.index].copertina);
      c.getdescrizione(info[e.index].descrizione);
      c.getisbn13(info[e.index].isbn13);
      c.getisbn10(info[e.index].isbn10);
      var window  = c.getView();
      window.title=info[e.index].titolo;
      window.open();

  });
