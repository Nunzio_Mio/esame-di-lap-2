// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var data=[];
var a1=[];
var info=[];
$.v1.show();
$.v2.hide();
var check=0;
var dataUsr=[];
var pRequest=require("parseCall");


var addBookF=function(title,book,user){
  pRequest.addParseBookWhitCheck(title,book,user);

}

function boh(){
  Ti.App.fireEvent("raimondo");
  $.win.close();
}

function utenti(e){
  $.v1.hide();
  $.v1.height=0;
  $.txt.hintText="Cerca un utente                              ";
  check=1;
  $.v2.height=Ti.UI.FILL;
  $.v2.show();
}

function libri(e){
  $.v1.height=Ti.UI.FILL;
  $.v1.show();
  $.txt.hintText="Cerca il tuo libro                           ";
  check=0;
  $.v2.height=0;
  $.v2.hide();

}

var serc= function(e){
  if(check==0){
  $.txt.blur();
  $.wait.getView().height=Ti.UI.FILL;
  $.wait.getView().width=Ti.UI.FILL;
  $.wait.getView().show();
  var titleSearch=$.txt.value.replace(" ","+");
  var search="https://www.googleapis.com/books/v1/volumes?q="+ titleSearch+"&orderBy=relevance";
  var web=Titanium.Network.createHTTPClient();
  web.setRequestHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
  web.open("GET",search);

  web.onload=function(){
    Ti.API.info("data json: "+this.responseText);
    data=JSON.parse(this.responseText);
    //if
    if(data["totalItems"]>0){
    for (var i = 0; i < 10; i++) {

      try{
      var cover=data["items"][i]["volumeInfo"]["imageLinks"].smallThumbnail;
    }catch(e){
      var cover="/images/notfound.png";
    }
      try{
      var titolo=data["items"][i]["volumeInfo"].title;
    }catch(e){
      var titolo="no Title";
    }

    if(typeof data["items"][i]["volumeInfo"].authors =='object'){
    var autore=data["items"][i]["volumeInfo"].authors[0];
  }else{
    var autore=data["items"][i]["volumeInfo"].authors;
  }
      try{
      var desc=data["items"][i]["volumeInfo"].description;
     }catch(e){
       var desc="Non Disponibile"
     }

      try{
      var isbn13=data["items"][i]["volumeInfo"].industryIdentifiers[0].identifier;
    }catch(e){
      var isbn13=-1;
    }

    try{
      var isbn10=data["items"][i]["volumeInfo"].industryIdentifiers[1].identifier;
    }catch(e){
      var isbn10=-1;
    }
      var row=Ti.UI.createTableViewRow();
      info[i]={
      titolo:titolo,
      autore:autore,
      copertina:cover,
      descrizione:desc,
      isbn13:isbn13,
      isbn10:isbn10
    };


    var v1=Ti.UI.createView();
      v1.borderColor="#e6e6e6";

      if(titolo.length>24){
        var t=titolo.slice(0,23);
        var newtit=t+"...";
      }
      else{
        var newtit=titolo;
      }



      var t2=Ti.UI.createLabel({
        text: autore,
        color: "black",
        left: "120px",
        top:30,
      });
      var t1=Ti.UI.createLabel({
        text: newtit,
        color: "black",
        left: "120px",
        top:10,
      });
      var c1=Ti.UI.createImageView({
        height:"150px",
        width:"100px",
        image:cover,
        left:5,
      });



      var plus=Ti.UI.createImageView({
        image:"/images/plus.png",
        right:7,
        height:"90px",
        width:"90px",

      });

      for(var j=0;j<Alloy.Globals.libri.length;j++){
        if(info[i].isbn10===Alloy.Globals.libri[j]){
          plus.image="/images/check.png";
        }
      }


      v1.add(plus);
      v1.add(t1);
      v1.add(t2);
      v1.add(c1);
      row.add(v1);
      plus.idrow=i;
      //a1.push(row);
      plus.addEventListener("click",function(e){
        var book={
          title:info[e.source.idrow].titolo,
          authors:info[e.source.idrow].autore,
          isbn:info[e.source.idrow].isbn10,
          isbn13:info[e.source.idrow].isbn13,
          desc:info[e.source.idrow].descrizione,
          imageName:info[e.source.idrow].copertina
        }
        pRequest.newFuncAdd(info[e.source.idrow].titolo,book,Parse.User.current());
        e.source.image="/images/check.png";

      });
      a1[i]=row;
  }
//fine if
}else{
  var failLabel=Ti.UI.createLabel({
    color:"black",
    text:"non ho trovato nulla",
  });
  $.books.add(failLabel);
}
$.books.setData(a1);
$.wait.getView().hide();
$.wait.getView().height=0;
$.wait.getView().width=0;
  }
  web.error=function(){
    Ti.API.info("Mi dispiace ma non troviamo il tuo libro :(");
    alert("Mi dispiace ma non troviamo il tuo libro :(");
  }
  web.send();
}else{
  Ti.API.info('---------------CERCO UTENTI--------');
  serc2();
}
}



  $.books.addEventListener("longclick",function(e){
      Ti.API.info(e.index);
      Ti.API.info(info[e.index].titolo);
      Ti.API.info(info[e.index].autore);
      Ti.API.info(info[e.index].copertina);
      Ti.API.info(info[e.index].descrizione);
      var c = Alloy.createController('bookpage');
      c.gettitolo(info[e.index].titolo);
      c.getscrittore(info[e.index].autore);
      c.getlibro(info[e.index].copertina);
      c.getdescrizione(info[e.index].descrizione);
      c.getisbn13(info[e.index].isbn13);
      c.getisbn10(info[e.index].isbn10);
      var window  = c.getView();
      window.title=info[e.index].titolo;
      window.open();

  });

var serc2=function() {
  $.txt.blur();
  $.wait2.getView().height=Ti.UI.FILL;
  $.wait2.getView().width=Ti.UI.FILL;
  $.wait2.getView().show();
  //Implementazione ricerca utente
  var q=new Parse.Query(Parse.User);
  q.equalTo("username", $.txt.value);
  q.find({
    success:function(results){
      if(results.length==0){
        var failLabel=Ti.UI.createLabel({
          color:"black",
          text:"non ho trovato nulla",
        });
        $.v2.add(failLabel);
      }else {
        for(var i=0; i<results.length;i++){
          var nome=results[i].get("username");
          var banner=results[i].get("banner");
          Ti.API.info('nome e banner : '+nome+ " "+banner);
          var b=pRequest.banner(banner);
          var count;
            Ti.API.info('-----------------------------');
            Ti.API.info('nome: '+nome);
            Ti.API.info('banner: '+b);
            //Ti.API.info('totali libri: '+foll.tot);

            var rel=results[i].relation("myBooks");
            var qe=rel.query();
            qe.equalTo("read", true);
            qe.count({
              success:function(letti){
                Ti.API.info('HO LETTO: '+letti);
                count=letti;
                var row=Ti.UI.createTableViewRow();
                var v3=Ti.UI.createView();
                v3.borderColor="gray";
                v3.height="150px"

                  var t4=Ti.UI.createLabel({
                    text: nome,
                    color: "black",
                    top:10,
                  });

                  Ti.API.info('count: '+count);

                  var t5=Ti.UI.createLabel({
                    text: "Ha letto: "+count+" libri!",
                    color: "gray",
                    bottom:10,
                  });

                  var c3=Ti.UI.createImageView({
                    height:"100px",
                    width:"100px",
                    borderRadius:40,
                    image:b,
                    left:5,
                  });

                  v3.add(c3);
                  v3.add(t4);
                  v3.add(t5);
                  row.add(v3);
                  row.addEventListener("click",function(e){
                    var a=Alloy.createController("usrpage");
                    a.dammiiltitolo(nome)
                    var won=a.getView();
                    won.setTitle(nome);
                    won.open();
                  });

    Ti.API.info('addirittura ho il cazzo duro e arrivo qua!');
              dataUsr[i]=row;
              $.qua.setData(dataUsr);
    Ti.API.info('datarow->'+dataUsr[i]);
    $.wait2.getView().hide();
    $.wait2.getView().height=0;
    $.wait2.getView().width=0;
              },
              error:function(error){
                Ti.API.info(error.errorState);
              }
            });
        }
      }
    }
  });
  /*$.wait2.getView().hide();
  $.wait2.getView().height=0;
  $.wait2.getView().width=0;*/
}
