// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var isbn10,isbn13;




this.gettitolo=function(e){
  $.utitulu.setText(e);
}

this.getscrittore=function(e){
  $.uscrittu.setText("Autore: "+e);
}

this.getlibro=function(e){
  $.ulibru.setImage(e);
}

this.getdescrizione=function(e){
  $.descru.setText(e);
}

this.getisbn10=function(e){
  isbn10=e;
  ratingF();
}


this.getisbn13=function(e){
  isbn13=e;
}

var addFun=function(title,book,user){
  var pRequest=require("parseCall");
  pRequest.addParseBookWhitCheck(title,book,user);
  /*var req={
    msg:"Invisible Monsters",
  }
  Parse.Cloud.run("sendAct",req);*/
}

var ratingF=function(){
  Ti.API.info('-------------entro in ratingf-----------');
  var bk=Parse.Object.extend("book");
	var rep_query = new Parse.Query(bk);
	rep_query.equalTo("isbn", isbn10);
  Ti.API.info('-------eseguo query '+isbn10+"-------------");
  rep_query.find({
    success: function(results){
      if(results.length!=0){
        var intr=results[0].get("rating")+"/5";
        $.rating.text=intr;
        Ti.API.info('----------------fine query------------');
      }
    }
  });
}
