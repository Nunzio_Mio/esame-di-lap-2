exports.view = "";

exports.banner = function(x) {
    switch (x) {
      case 1:
        var y = "/images/banner/fantascienza.jpg";
        break;

      case 2:
        var y = "/images/banner/fantasy.jpg";
        break;

      case 3:
        var y = "/images/banner/giallo.jpg";
        break;

      case 4:
        var y = "/images/banner/horror.jpg";
        break;

      case 5:
        var y = "/images/banner/rosa.jpg";
        break;

      default:
        var y = "/images/banner/banner-default.jpg";
    }
    return y;
};

exports.signUp = function(name, pass, Email) {
    Ti.API.info("Inizio signUp");
    var currentUser = Parse.User.current();
    Ti.API.info("Assegno currentUser");
    currentUser && Parse.User.logOut();
    Ti.API.info("fine controllo user");
    var login = name;
    var password = pass;
    var user_origin = Parse.User.extend("userp");
    var user = new user_origin();
    user.set("username", login);
    user.set("password", password);
    user.set("email", Email);
    user.set("banner", 0);
    user.set("cit", "Nessuna citazione inserita, vai su modifica profilo per inserirla !");
    Ti.API.info("Inizio Parse SigUp");
    user.signUp(null, {
        success: function(user) {
            Ti.API.info("name:" + user);
            Ti.API.info("Token Salvata->" + Ti.App.Properties.getString("deviceToken"));
            exports.toParseInstall(Ti.App.Properties.getString("deviceToken"), "android", Parse.User.current().id);
            Alloy.createController("bckgnd").getView().open();
        },
        error: function(user, error) {
            Ti.API.info("Errore-> " + error.code + " " + error.message);
            202 == error.code && alert("User già preso");
        }
    });
};

exports.logIn = function(name, pass) {
    Ti.API.info("inizio logIn");
    var currentUser = Parse.User.current();
    Ti.API.info("Assegnio currentUser");
    currentUser && Parse.User.logOut();
    Ti.API.info("fine controllo user");
    var login = name;
    var password = pass;
    var user = new Parse.User();
    user.set("username", login);
    user.set("password", password);
    Ti.API.info("inzio Parse logIn");
    Parse.User.logIn(login, password, {
        success: function(user) {
            Ti.API.info("user: " + user.getUsername());
            exports.toParseInstall(Ti.App.Properties.getString("deviceToken"), "android", Parse.User.current().id);
            Alloy.createController("bckgnd").getView().open();
        },
        error: function(user, error) {
            Ti.API.info("Errore->" + error.code + " " + error.message);
        }
    });
};

exports.toParseInstall = function(deviceToken, device, id) {
    Ti.API.info("inizio toParseInstall");
    var httpC = Ti.Network.createHTTPClient();
    Ti.API.info("url->" + Alloy.CFG.parseOptions.parseServerURL);
    var url = Alloy.CFG.parseOptions.parseServerURL + "/Installations/";
    Ti.API.info("Preparo funzioni");
    httpC.onload = function(e) {
        if (e.success) {
            Ti.API.info("Risposta->" + httpC.responseText);
            Ti.API.info("installations OK");
        }
    };
    httpC.onerror = function(e) {
        alert("Errore installations");
        Ti.API.info(e.code);
    };
    httpC.open("POST", url);
    Ti.API.info("aperto");
    httpC.setRequestHeader("X-Parse-Application-Id", Alloy.CFG.parseOptions.applicationId);
    httpC.setRequestHeader("X-Parse-Master-Key", Alloy.CFG.parseOptions.javascriptkey);
    httpC.setRequestHeader("Content-Type", "application/json");
    Ti.API.info("SettoRequest");
    var data = {
        deviceType: device,
        deviceToken: deviceToken,
        GCMSenderId: "114323755074",
        pushType: "gcm",
        channels: [ "" ],
        user: id
    };
    Ti.API.info("invio");
    httpC.send(JSON.stringify(data));
};

exports.toGoogleReg = function() {
    var instG = require("ti.goosh");
    Ti.API.info("il conteiner alla prima istanza->" + exports.view);
    instG.registerForPushNotifications({
        senderId: "114323755074",
        callback: function(e) {
            e.data = JSON.parse(e.data);
            Ti.API.info("e è :" + toString.data.get("alert"));
            var params = {
                uname: "",
                nbook: ""
            };
            var notif = Alloy.createController("notifRow", params).getView();
            exports.view.addElm(notif);
            Ti.API.info("la row->" + notif);
            Ti.API.info("Fine della push");
        },
        success: function(e) {
            Ti.API.info("il token->" + e.deviceToken);
            Ti.App.Properties.setString("deviceToken", e.deviceToken);
        },
        error: function(e, error) {
            Ti.API.info("Errore->" + error.code + " " + error.message);
        }
    });
};

exports.addComment = function(comment, user, book) {
    var commentParse = Parse.Object.extend("commentModel");
    var actualMessg = new commentParse();
    var usr = user.getUsername();
    actualMessg.save({
        user: usr,
        book: book.title,
        commenttext: comment
    }, {
        success: function() {
            alert("Postato");
        },
        error: function(error) {
            Ti.API.info("Errore post: " + error.code + " " + error.message);
        }
    });
};

exports.addBook = function(book) {
    var bookParse = Parse.Object.extend("book");
    var actBook = new bookParse();
    actBook.save({
        title: book.title,
        isbn: book.isbn,
        isbn13: book.isbn13,
        authors: book.authors,
        imageName: book.imageName,
        desc: book.desc,
        rating: 0
    }, {
        success: function(book) {
            Ti.API.info("Libro inserito->" + book.id);
            exports.addmyBook(book.id);
        },
        error: function(error) {
            Ti.API.info("Errore addBook: " + error.code + " " + error.message);
        }
    });
};

exports.addmyBook = function(bookId) {
    var bookParse = Parse.Object.extend("myBook");
    var usr = new bookParse();
    var myBook = {
        book: {
            __type: "Pointer",
            className: "book",
            objectId: bookId
        },
        favorite: false,
        read: false,
        myRating: 0
    };
    usr.save(myBook, {
        success: function(myBook) {
            alert("addmyBook ok!");
            var usr = Parse.User.current();
            var rel = usr.relation("myBooks");
            rel.add(myBook);
            usr.save();
        },
        error: function(error) {
            Ti.API.info("Errore addmyBook: " + error.code + " " + error.message);
        }
    });
};

exports.addParseBookWhitCheck = function(title, book, user) {
    var bookParse = Parse.Object.extend("book");
    var query = new Parse.Query(bookParse);
    query.equalTo("title", title);
    query.find().then(function(results) {
        if (0 == results.length) {
            Ti.API.info("Trovato nulla");
            exports.addBook(book, user);
        } else {
            Ti.API.info("trovato->" + results);
            var id = results[0].id;
            Ti.API.info("id2->" + id);
            Ti.API.info("Pointer->" + results[0].toPointer());
            exports.addmyBook(results[0].id);
        }
    });
};

exports.newFuncAdd = function(title, book, user) {
    exports.addParseBookWhitCheck(title, book, user);
};

exports.addmyB = function(myB) {
    var usr = Parse.User.current();
    usr.add("myB", myB);
    usr.save();
};

exports.retriveComment = function() {
    var commentParse = Parse.Object.extend("commentModel");
    var query = new Parse.Query(commentParse);
    query.find({
        success: function(results) {
            return results;
        },
        error: function() {
            Ti.API.info("Error Retrive comment");
        }
    });
};

exports.setRead = function(obId) {
    var obj = Parse.Object.extend("myBook");
    var query = new Parse.Query(obj);
    query.get(obId, {
        success: function() {
            Ti.API.info("riuscito");
        },
        error: function(error) {
            Ti.API.info("non funziona->" + error.code + " " + error.message);
        }
    }).then(function(results) {
        Ti.API.info("entro nel then");
        Ti.API.info("Res->" + results.id);
        results.save({
            read: true
        }, {
            success: function() {
                Ti.API.info("Save riuscita");
            },
            error: function(error) {
                Ti.API.info("Save non fuziona->" + error.code + " " + error.message);
            }
        });
    });
};

exports.addratingMy = function(obId, rat) {
    var obj = Parse.Object.extend("myBook");
    var query = new Parse.Query(obj);
    query.get(obId, {
        success: function() {
            Ti.API.info("riuscito");
        },
        error: function(error) {
            Ti.API.info("non fuziona->" + error.code + " " + error.message);
        }
    }).then(function(results) {
        Ti.API.info("entro nel then");
        Ti.API.info("Res->" + results.id);
        results.save({
            myRating: rat
        }, {
            success: function() {
                Ti.API.info("rat->" + rat);
                Ti.API.info("Save riuscita");
            },
            error: function(error) {
                Ti.API.info("Save non fuziona->" + error.code + " " + error.message);
            }
        });
    });
};

exports.retriveComment = function(bookTitle) {
    var commentParse = Parse.Object.extend("commentModel");
    var query = new Parse.Query(commentParse);
    query.equalTo("book", bookTitle);
    query.find({
        success: function(results) {
            return results;
        },
        error: function() {
            Ti.API.info("Error Retrive comment");
        }
    });
};

exports.setFav = function(obId) {
    var obj = Parse.Object.extend("myBook");
    var query = new Parse.Query(obj);
    query.get(obId, {
        success: function() {
            Ti.API.info("riuscito");
        },
        error: function(error) {
            Ti.API.info("non fuziona->" + error.code + " " + error.message);
        }
    }).then(function(results) {
        Ti.API.info("entro nel then");
        Ti.API.info("Res->" + results.id);
        results.save({
            favorite: true
        }, {
            success: function() {
                Ti.API.info("Save riuscita");
            },
            error: function(error) {
                Ti.API.info("Save non fuziona->" + error.code + " " + error.message);
            }
        });
    });
};

exports.retriveUserProfle = function(usr) {
    var bookParse = Parse.Object.extend("User");
    var query = new Parse.Query(bookParse);
    query.equalTo("useraname", usr);
    query.find().then(function(results) {
        var us = results;
        var rel = us.relation("myBooks");
        var q = rel.query();
        q.find().then(function(results) {
            for (var i = 0; i < results.length; i++) results[i].get("read") || results[i].get("favorite");
        });
    });
};