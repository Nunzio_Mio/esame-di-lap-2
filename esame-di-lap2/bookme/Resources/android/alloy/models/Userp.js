var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

exports.definition = {
    config: {
        columns: {
            nick: "text",
            birth: "date",
            cit: "text",
            ImageProf: "text"
        },
        adapter: {
            type: "sql",
            collection_name: "userp"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M("userp", exports.definition, []);

collection = Alloy.C("userp", exports.definition, model);

exports.Model = model;

exports.Collection = collection;