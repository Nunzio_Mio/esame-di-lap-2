var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

exports.definition = {
    config: {
        columns: {
            user: "text",
            book: "text",
            commenttext: "text",
            like: "integer"
        },
        adapter: {
            type: "sql",
            collection_name: "commentModel"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M("commentModel", exports.definition, []);

collection = Alloy.C("commentModel", exports.definition, model);

exports.Model = model;

exports.Collection = collection;