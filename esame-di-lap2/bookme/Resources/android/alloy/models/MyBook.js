var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

exports.definition = {
    config: {
        columns: {
            book: {
                __type: "string",
                className: "string",
                objectId: "string"
            },
            favorite: "boolean",
            read: "boolean",
            myRating: "string"
        },
        adapter: {
            type: "properties",
            collection_name: "myBook"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

model = Alloy.M("myBook", exports.definition, []);

collection = Alloy.C("myBook", exports.definition, model);

exports.Model = model;

exports.Collection = collection;