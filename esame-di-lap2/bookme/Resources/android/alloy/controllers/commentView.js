function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function AllRisp() {
        var cla = Parse.Object.extend("commentModel");
        var rep_query = new Parse.Query(cla);
        rep_query.equalTo("user", utente);
        rep_query.equalTo("book", libro);
        rep_query.find({
            success: function(results) {
                Ti.API.info("Success numero tipo uno");
                comment = results[0];
                var rel = comment.relation("responseRel");
                var q = rel.query();
                q.ascending("createdAt");
                q.find({
                    success: function(results) {
                        if (0 == results.length) {
                            var l = Ti.UI.createLabel({
                                text: "Non ci sono risposte!",
                                color: "black"
                            });
                            $.lv.add(l);
                        } else for (var i = 0; i < results.length; i++) {
                            var us = results[i].get("user");
                            Ti.API.info("USERRRRRRRRRRR: " + us);
                            var co = results[i].get("textResponse");
                            Ti.API.info("COMMENTOOOOOOO: " + co);
                            var dat = results[i].get("createdAt");
                            Ti.API.info("LA DATAAAAAAAA: " + dat);
                            var row = Ti.UI.createTableViewRow();
                            var v = Ti.UI.createView({
                                height: Ti.UI.SiZE,
                                layout: "vertical",
                                borderColor: "#224d72"
                            });
                            Ti.API.info("-----DOPO La VIEW-----------------");
                            var t = Ti.UI.createLabel({
                                text: us,
                                color: "black",
                                top: 10,
                                left: 5
                            });
                            Ti.API.info("-----DOPO La T-----------------");
                            var t1 = Ti.UI.createLabel({
                                text: "'' " + co + " ''",
                                color: "black",
                                top: 15,
                                left: 10,
                                right: 10
                            });
                            Ti.API.info("-----DOPO La T1-----------------");
                            var dat2 = dat.toString().slice(0, 21);
                            Ti.API.info("--------DAT2->" + dat2);
                            var t2 = Ti.UI.createLabel({
                                text: dat2,
                                color: "gray",
                                bottom: 5,
                                right: 5
                            });
                            Ti.API.info("-----------arrivo qui---------");
                            v.add(t);
                            v.add(t1);
                            v.add(t2);
                            row.add(v);
                            dt[i] = row;
                            $.list.setData(dt);
                            Ti.API.info("------------alla fine--------------");
                        }
                    }
                });
            }
        });
    }
    function ricommenta() {
        var cur = Parse.User.current().get("username");
        var data = [];
        data.push(utente);
        data.push(libro);
        data.push(cur);
        data.push(libro);
        data.push($.txt.value);
        var req = {
            vett: data
        };
        Parse.Cloud.run("addResponse", req);
        var mess = cur + " ha risposto al tuo commento su " + libro;
        var req2 = {
            msg: mess,
            usr: utente
        };
        Parse.Cloud.run("sendNotifResp", req2);
        $.txt.blur();
        var row = Ti.UI.createTableViewRow();
        var v = Ti.UI.createView({
            height: Ti.UI.SiZE,
            layout: "vertical",
            borderColor: "#224d72"
        });
        Ti.API.info("-----DOPO La VIEW-----------------");
        var t = Ti.UI.createLabel({
            text: cur,
            color: "black",
            top: 10,
            left: 5
        });
        Ti.API.info("-----DOPO La T-----------------");
        var co = $.txt.value;
        var t1 = Ti.UI.createLabel({
            text: "'' " + co + " ''",
            color: "black",
            top: 15,
            left: 10,
            right: 10
        });
        Ti.API.info("-----DOPO La T1-----------------");
        var t2 = Ti.UI.createLabel({
            text: "now",
            color: "gray",
            bottom: 5,
            right: 5
        });
        Ti.API.info("-----------arrivo qui---------");
        v.add(t);
        v.add(t1);
        v.add(t2);
        row.add(v);
        $.list.appendRow(row);
        $.txt.value = "";
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "commentView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.__alloyId5 = Ti.UI.createView({
        width: "100%",
        height: "70%",
        backgroundColor: "white",
        zIndex: 1e4,
        layout: "vertical",
        id: "__alloyId5"
    });
    $.__views.lv = Ti.UI.createView({
        id: "lv",
        height: "900px"
    });
    $.__views.__alloyId5.add($.__views.lv);
    $.__views.list = Ti.UI.createTableView({
        id: "list"
    });
    $.__views.lv.add($.__views.list);
    $.__views.__alloyId6 = Ti.UI.createView({
        id: "__alloyId6"
    });
    $.__views.__alloyId5.add($.__views.__alloyId6);
    $.__views.__alloyId7 = Ti.UI.createLabel({
        height: 2,
        width: "94%",
        bottom: "30px",
        top: "30px",
        backgroundColor: "#22728f",
        id: "__alloyId7"
    });
    $.__views.__alloyId6.add($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createView({
        id: "__alloyId8"
    });
    $.__views.__alloyId5.add($.__views.__alloyId8);
    $.__views.txt = Ti.UI.createTextField({
        id: "txt",
        color: "black",
        bottom: "20px",
        left: 10,
        hintTextColor: "gray",
        hintText: "Scrivi il tuo commento...",
        width: "70%"
    });
    $.__views.__alloyId8.add($.__views.txt);
    $.__views.__alloyId9 = Ti.UI.createImageView({
        image: "/images/edit.png",
        bottom: "30px",
        right: 10,
        id: "__alloyId9"
    });
    $.__views.__alloyId8.add($.__views.__alloyId9);
    ricommenta ? $.addListener($.__views.__alloyId9, "click", ricommenta) : __defers["$.__views.__alloyId9!click!ricommenta"] = true;
    $.__views.commentView = Ti.UI.createAlertDialog({
        width: "100%",
        height: "70%",
        backgroundColor: "white",
        zIndex: 1e4,
        androidView: $.__views.__alloyId5,
        id: "commentView"
    });
    $.__views.commentView && $.addTopLevelView($.__views.commentView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var utente = args.user;
    var libro = args.libro;
    args.comm;
    var dt = [];
    AllRisp();
    __defers["$.__views.__alloyId9!click!ricommenta"] && $.addListener($.__views.__alloyId9, "click", ricommenta);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;