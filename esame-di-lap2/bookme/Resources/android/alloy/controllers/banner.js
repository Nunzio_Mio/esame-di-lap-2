function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function choose() {
        var bnnr = Alloy.createController("change_me");
        bnnr.changeBan("/images/banner/horror.jpg", 4, img, cit);
        var win = bnnr.getView();
        win.open();
    }
    function chooseu() {
        var bnnr = Alloy.createController("change_me");
        bnnr.changeBan("/images/banner/rosa.jpg", 5, img, cit);
        var win = bnnr.getView();
        win.open();
    }
    function choosed() {
        var bnnr = Alloy.createController("change_me");
        bnnr.changeBan("/images/banner/giallo.jpg", 3, img, cit);
        var win = bnnr.getView();
        win.open();
    }
    function chooset() {
        var bnnr = Alloy.createController("change_me");
        bnnr.changeBan("/images/banner/fantasy.jpg", 2, img, cit);
        var win = bnnr.getView();
        win.open();
    }
    function chooseq() {
        var bnnr = Alloy.createController("change_me");
        bnnr.changeBan("/images/banner/fantascienza.jpg", 1, img, cit);
        var win = bnnr.getView();
        win.open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "banner";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.banner = Ti.UI.createWindow({
        backgroundColor: "white",
        title: "Scegli il tuo Banner",
        id: "banner"
    });
    $.__views.banner && $.addTopLevelView($.__views.banner);
    $.__views.__alloyId1 = Ti.UI.createScrollView({
        layout: "vertical",
        id: "__alloyId1"
    });
    $.__views.banner.add($.__views.__alloyId1);
    $.__views.v1 = Ti.UI.createView({
        height: "20%",
        borderColor: "gray",
        id: "v1",
        backgroundImage: "/images/banner/horror.jpg"
    });
    $.__views.__alloyId1.add($.__views.v1);
    choose ? $.addListener($.__views.v1, "click", choose) : __defers["$.__views.v1!click!choose"] = true;
    $.__views.v2 = Ti.UI.createView({
        height: "20%",
        borderColor: "gray",
        id: "v2",
        backgroundImage: "/images/banner/rosa.jpg"
    });
    $.__views.__alloyId1.add($.__views.v2);
    chooseu ? $.addListener($.__views.v2, "click", chooseu) : __defers["$.__views.v2!click!chooseu"] = true;
    $.__views.v3 = Ti.UI.createView({
        height: "20%",
        borderColor: "gray",
        id: "v3",
        backgroundImage: "/images/banner/giallo.jpg"
    });
    $.__views.__alloyId1.add($.__views.v3);
    choosed ? $.addListener($.__views.v3, "click", choosed) : __defers["$.__views.v3!click!choosed"] = true;
    $.__views.v4 = Ti.UI.createView({
        height: "20%",
        borderColor: "gray",
        id: "v4",
        backgroundImage: "/images/banner/fantasy.jpg"
    });
    $.__views.__alloyId1.add($.__views.v4);
    chooset ? $.addListener($.__views.v4, "click", chooset) : __defers["$.__views.v4!click!chooset"] = true;
    $.__views.v5 = Ti.UI.createView({
        height: "20%",
        borderColor: "gray",
        id: "v5",
        backgroundImage: "/images/banner/fantascienza.jpg"
    });
    $.__views.__alloyId1.add($.__views.v5);
    chooseq ? $.addListener($.__views.v5, "click", chooseq) : __defers["$.__views.v5!click!chooseq"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = $.args;
    var img = args[0];
    var cit = args[1];
    __defers["$.__views.v1!click!choose"] && $.addListener($.__views.v1, "click", choose);
    __defers["$.__views.v2!click!chooseu"] && $.addListener($.__views.v2, "click", chooseu);
    __defers["$.__views.v3!click!choosed"] && $.addListener($.__views.v3, "click", choosed);
    __defers["$.__views.v4!click!chooset"] && $.addListener($.__views.v4, "click", chooset);
    __defers["$.__views.v5!click!chooseq"] && $.addListener($.__views.v5, "click", chooseq);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;