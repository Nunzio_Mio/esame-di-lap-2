function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId53() {
        $.__views.win.removeEventListener("open", __alloyId53);
        if ($.__views.win.activity) $.__views.win.activity.actionBar.homeButtonEnabled = true; else {
            Ti.API.warn("You attempted to access an Activity on a lightweight Window or other");
            Ti.API.warn("UI component which does not have an Android activity. Android Activities");
            Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows.");
        }
    }
    function __alloyId59() {
        $.__views.win.removeEventListener("open", __alloyId59);
        if ($.__views.win.activity) $.__views.win.activity.onCreateOptionsMenu = function(e) {
            $.__views.txt = Ti.UI.createTextField({
                width: "70%",
                color: "white",
                hintText: "Cerca il tuo libro                           ",
                hintTextColor: "gray",
                returnKeyType: Ti.UI.RETURNKEY_SEARCH,
                id: "txt"
            });
            serc ? $.addListener($.__views.txt, "return", serc) : __defers["$.__views.txt!return!serc"] = true;
            $.txt = $.__views.txt;
            var __alloyId56 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                id: "__alloyId55"
            };
            $.__views.txt && (__alloyId56.actionView = $.__views.txt);
            $.__views.__alloyId55 = e.menu.add(_.pick(__alloyId56, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId55.applyProperties(_.omit(__alloyId56, Alloy.Android.menuItemCreateArgs));
            $.__alloyId55 = $.__views.__alloyId55;
            var __alloyId58 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                icon: "/s1.png",
                id: "__alloyId57"
            };
            $.__views.__alloyId57 = e.menu.add(_.pick(__alloyId58, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId57.applyProperties(_.omit(__alloyId58, Alloy.Android.menuItemCreateArgs));
            $.__alloyId57 = $.__views.__alloyId57;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function boh() {
        Ti.App.fireEvent("raimondo");
        $.win.close();
    }
    function utenti() {
        $.v1.hide();
        $.v1.height = 0;
        $.txt.hintText = "Cerca un utente                              ";
        check = 1;
        $.v2.height = Ti.UI.FILL;
        $.v2.show();
    }
    function libri() {
        $.v1.height = Ti.UI.FILL;
        $.v1.show();
        $.txt.hintText = "Cerca il tuo libro                           ";
        check = 0;
        $.v2.height = 0;
        $.v2.hide();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "searchView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        title: "",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    boh ? $.addListener($.__views.win, "androidback", boh) : __defers["$.__views.win!androidback!boh"] = true;
    $.__views.win.addEventListener("open", __alloyId53);
    $.__views.win.addEventListener("open", __alloyId59);
    $.__views.btnm = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "btnm",
        layout: "horizontal"
    });
    $.__views.win.add($.__views.btnm);
    $.__views.__alloyId60 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        color: "white",
        title: "Libri",
        left: 0,
        width: "50%",
        id: "__alloyId60"
    });
    $.__views.btnm.add($.__views.__alloyId60);
    libri ? $.addListener($.__views.__alloyId60, "click", libri) : __defers["$.__views.__alloyId60!click!libri"] = true;
    $.__views.__alloyId61 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        color: "white",
        title: "Utenti",
        right: 0,
        width: "50%",
        left: 0,
        id: "__alloyId61"
    });
    $.__views.btnm.add($.__views.__alloyId61);
    utenti ? $.addListener($.__views.__alloyId61, "click", utenti) : __defers["$.__views.__alloyId61!click!utenti"] = true;
    $.__views.v1 = Ti.UI.createView({
        height: Ti.UI.FILL,
        id: "v1"
    });
    $.__views.win.add($.__views.v1);
    $.__views.wait = Alloy.createController("waitView", {
        id: "wait",
        __parentSymbol: $.__views.v1
    });
    $.__views.wait.setParent($.__views.v1);
    $.__views.books = Ti.UI.createTableView({
        id: "books"
    });
    $.__views.v1.add($.__views.books);
    $.__views.v2 = Ti.UI.createView({
        height: Ti.UI.FILL,
        id: "v2"
    });
    $.__views.win.add($.__views.v2);
    $.__views.wait2 = Alloy.createController("waitView", {
        id: "wait2",
        __parentSymbol: $.__views.v2
    });
    $.__views.wait2.setParent($.__views.v2);
    $.__views.qua = Ti.UI.createTableView({
        id: "qua"
    });
    $.__views.v2.add($.__views.qua);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var data = [];
    var a1 = [];
    var info = [];
    $.v1.show();
    $.v2.hide();
    var check = 0;
    var dataUsr = [];
    var pRequest = require("parseCall");
    var serc = function() {
        if (0 == check) {
            $.txt.blur();
            $.wait.getView().height = Ti.UI.FILL;
            $.wait.getView().width = Ti.UI.FILL;
            $.wait.getView().show();
            var titleSearch = $.txt.value.replace(" ", "+");
            var search = "https://www.googleapis.com/books/v1/volumes?q=" + titleSearch + "&orderBy=relevance";
            var web = Titanium.Network.createHTTPClient();
            web.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
            web.open("GET", search);
            web.onload = function() {
                Ti.API.info("data json: " + this.responseText);
                data = JSON.parse(this.responseText);
                if (data["totalItems"] > 0) for (var i = 0; 10 > i; i++) {
                    try {
                        var cover = data["items"][i]["volumeInfo"]["imageLinks"].smallThumbnail;
                    } catch (e) {
                        var cover = "/images/notfound.png";
                    }
                    try {
                        var titolo = data["items"][i]["volumeInfo"].title;
                    } catch (e) {
                        var titolo = "no Title";
                    }
                    if ("object" == typeof data["items"][i]["volumeInfo"].authors) var autore = data["items"][i]["volumeInfo"].authors[0]; else var autore = data["items"][i]["volumeInfo"].authors;
                    try {
                        var desc = data["items"][i]["volumeInfo"].description;
                    } catch (e) {
                        var desc = "Non Disponibile";
                    }
                    try {
                        var isbn13 = data["items"][i]["volumeInfo"].industryIdentifiers[0].identifier;
                    } catch (e) {
                        var isbn13 = -1;
                    }
                    try {
                        var isbn10 = data["items"][i]["volumeInfo"].industryIdentifiers[1].identifier;
                    } catch (e) {
                        var isbn10 = -1;
                    }
                    var row = Ti.UI.createTableViewRow();
                    info[i] = {
                        titolo: titolo,
                        autore: autore,
                        copertina: cover,
                        descrizione: desc,
                        isbn13: isbn13,
                        isbn10: isbn10
                    };
                    var v1 = Ti.UI.createView();
                    v1.borderColor = "#e6e6e6";
                    if (titolo.length > 24) {
                        var t = titolo.slice(0, 23);
                        var newtit = t + "...";
                    } else var newtit = titolo;
                    var t2 = Ti.UI.createLabel({
                        text: autore,
                        color: "black",
                        left: "120px",
                        top: 30
                    });
                    var t1 = Ti.UI.createLabel({
                        text: newtit,
                        color: "black",
                        left: "120px",
                        top: 10
                    });
                    var c1 = Ti.UI.createImageView({
                        height: "150px",
                        width: "100px",
                        image: cover,
                        left: 5
                    });
                    var plus = Ti.UI.createImageView({
                        image: "/images/plus.png",
                        right: 7,
                        height: "90px",
                        width: "90px"
                    });
                    for (var j = 0; j < Alloy.Globals.libri.length; j++) info[i].isbn10 === Alloy.Globals.libri[j] && (plus.image = "/images/check.png");
                    v1.add(plus);
                    v1.add(t1);
                    v1.add(t2);
                    v1.add(c1);
                    row.add(v1);
                    plus.idrow = i;
                    plus.addEventListener("click", function(e) {
                        var book = {
                            title: info[e.source.idrow].titolo,
                            authors: info[e.source.idrow].autore,
                            isbn: info[e.source.idrow].isbn10,
                            isbn13: info[e.source.idrow].isbn13,
                            desc: info[e.source.idrow].descrizione,
                            imageName: info[e.source.idrow].copertina
                        };
                        pRequest.newFuncAdd(info[e.source.idrow].titolo, book, Parse.User.current());
                        e.source.image = "/images/check.png";
                    });
                    a1[i] = row;
                } else {
                    var failLabel = Ti.UI.createLabel({
                        color: "black",
                        text: "non ho trovato nulla"
                    });
                    $.books.add(failLabel);
                }
                $.books.setData(a1);
                $.wait.getView().hide();
                $.wait.getView().height = 0;
                $.wait.getView().width = 0;
            };
            web.error = function() {
                Ti.API.info("Mi dispiace ma non troviamo il tuo libro :(");
                alert("Mi dispiace ma non troviamo il tuo libro :(");
            };
            web.send();
        } else {
            Ti.API.info("---------------CERCO UTENTI--------");
            serc2();
        }
    };
    $.books.addEventListener("longclick", function(e) {
        Ti.API.info(e.index);
        Ti.API.info(info[e.index].titolo);
        Ti.API.info(info[e.index].autore);
        Ti.API.info(info[e.index].copertina);
        Ti.API.info(info[e.index].descrizione);
        var c = Alloy.createController("bookpage");
        c.gettitolo(info[e.index].titolo);
        c.getscrittore(info[e.index].autore);
        c.getlibro(info[e.index].copertina);
        c.getdescrizione(info[e.index].descrizione);
        c.getisbn13(info[e.index].isbn13);
        c.getisbn10(info[e.index].isbn10);
        var window = c.getView();
        window.title = info[e.index].titolo;
        window.open();
    });
    var serc2 = function() {
        $.txt.blur();
        $.wait2.getView().height = Ti.UI.FILL;
        $.wait2.getView().width = Ti.UI.FILL;
        $.wait2.getView().show();
        var q = new Parse.Query(Parse.User);
        q.equalTo("username", $.txt.value);
        q.find({
            success: function(results) {
                if (0 == results.length) {
                    var failLabel = Ti.UI.createLabel({
                        color: "black",
                        text: "non ho trovato nulla"
                    });
                    $.v2.add(failLabel);
                } else for (var i = 0; i < results.length; i++) {
                    var nome = results[i].get("username");
                    var banner = results[i].get("banner");
                    Ti.API.info("nome e banner : " + nome + " " + banner);
                    var b = pRequest.banner(banner);
                    var count;
                    Ti.API.info("-----------------------------");
                    Ti.API.info("nome: " + nome);
                    Ti.API.info("banner: " + b);
                    var rel = results[i].relation("myBooks");
                    var qe = rel.query();
                    qe.equalTo("read", true);
                    qe.count({
                        success: function(letti) {
                            Ti.API.info("HO LETTO: " + letti);
                            count = letti;
                            var row = Ti.UI.createTableViewRow();
                            var v3 = Ti.UI.createView();
                            v3.borderColor = "gray";
                            v3.height = "150px";
                            var t4 = Ti.UI.createLabel({
                                text: nome,
                                color: "black",
                                top: 10
                            });
                            Ti.API.info("count: " + count);
                            var t5 = Ti.UI.createLabel({
                                text: "Ha letto: " + count + " libri!",
                                color: "gray",
                                bottom: 10
                            });
                            var c3 = Ti.UI.createImageView({
                                height: "100px",
                                width: "100px",
                                borderRadius: 40,
                                image: b,
                                left: 5
                            });
                            v3.add(c3);
                            v3.add(t4);
                            v3.add(t5);
                            row.add(v3);
                            row.addEventListener("click", function() {
                                var a = Alloy.createController("usrpage");
                                a.dammiiltitolo(nome);
                                var won = a.getView();
                                won.setTitle(nome);
                                won.open();
                            });
                            Ti.API.info("addirittura ho il cazzo duro e arrivo qua!");
                            dataUsr[i] = row;
                            $.qua.setData(dataUsr);
                            Ti.API.info("datarow->" + dataUsr[i]);
                            $.wait2.getView().hide();
                            $.wait2.getView().height = 0;
                            $.wait2.getView().width = 0;
                        },
                        error: function(error) {
                            Ti.API.info(error.errorState);
                        }
                    });
                }
            }
        });
    };
    __defers["$.__views.win!androidback!boh"] && $.addListener($.__views.win, "androidback", boh);
    __defers["$.__views.txt!return!serc"] && $.addListener($.__views.txt, "return", serc);
    __defers["$.__views.__alloyId60!click!libri"] && $.addListener($.__views.__alloyId60, "click", libri);
    __defers["$.__views.__alloyId61!click!utenti"] && $.addListener($.__views.__alloyId61, "click", utenti);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;