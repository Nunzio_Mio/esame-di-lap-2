function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "notifRow";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.nRow = Ti.UI.createTableViewRow({
        backgroundColor: "gray",
        opacity: .4,
        layout: "vertical",
        id: "nRow"
    });
    $.__views.nRow && $.addTopLevelView($.__views.nRow);
    $.__views.name = Ti.UI.createLabel({
        color: "black",
        text: "prova",
        id: "name"
    });
    $.__views.nRow.add($.__views.name);
    $.__views.nbook = Ti.UI.createLabel({
        color: "black",
        text: "prova",
        id: "nbook"
    });
    $.__views.nRow.add($.__views.nbook);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    Ti.API.info("i dati della row->" + args.uname);
    $.name.text = args.uname;
    $.nbook.text = args.nbook;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;