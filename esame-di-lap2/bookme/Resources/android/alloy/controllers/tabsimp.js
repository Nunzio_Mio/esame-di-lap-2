function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function Comm() {
        var c = Alloy.createController("simposio");
        var r = c.getView();
        $.v.add(r);
    }
    function Follo() {}
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "tabsimp";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.tabsimp = Ti.UI.createWindow({
        id: "tabsimp",
        layout: "vertical"
    });
    $.__views.tabsimp && $.addTopLevelView($.__views.tabsimp);
    $.__views.__alloyId68 = Ti.UI.createView({
        id: "__alloyId68"
    });
    $.__views.tabsimp.add($.__views.__alloyId68);
    $.__views.__alloyId69 = Ti.UI.createButton({
        backgroundColor: "#22d472",
        title: "Commenti",
        top: 0,
        left: 0,
        width: "50%",
        id: "__alloyId69"
    });
    $.__views.__alloyId68.add($.__views.__alloyId69);
    Comm ? $.addListener($.__views.__alloyId69, "click", Comm) : __defers["$.__views.__alloyId69!click!Comm"] = true;
    $.__views.__alloyId70 = Ti.UI.createButton({
        backgroundColor: "#22d472",
        title: "Following",
        top: 0,
        right: 0,
        width: "50%",
        id: "__alloyId70"
    });
    $.__views.__alloyId68.add($.__views.__alloyId70);
    Follo ? $.addListener($.__views.__alloyId70, "click", Follo) : __defers["$.__views.__alloyId70!click!Follo"] = true;
    $.__views.v = Ti.UI.createView({
        id: "v"
    });
    $.__views.tabsimp.add($.__views.v);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    __defers["$.__views.__alloyId69!click!Comm"] && $.addListener($.__views.__alloyId69, "click", Comm);
    __defers["$.__views.__alloyId70!click!Follo"] && $.addListener($.__views.__alloyId70, "click", Follo);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;