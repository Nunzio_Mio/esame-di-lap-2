function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function save() {
        Ti.API.info("inizio save");
        if ("/images/check.png" != $.f.image) {
            Ti.API.info("modifico immagine profilo");
            var version = Ti.Platform.getVersion();
            var ver = parseInt(version);
            Ti.API.info("os version" + version);
            if (ver > 5) {
                Ti.API.info("versione>5");
                if (true == Ti.Filesystem.hasStoragePermissions()) {
                    Ti.API.info("Ho i permessi");
                    saveImm();
                } else {
                    Ti.API.info("Non ho i permessi");
                    Ti.Filesystem.requestStoragePermissions(function() {
                        saveImm();
                    });
                }
            } else {
                Ti.API.info("versione<5");
                saveImm();
                if ("" != $.W.value) {
                    usr.set("cit", $.W.value);
                    usr.save();
                }
                if (0 != typebanner) {
                    usr.set("banner", typebanner);
                    usr.save();
                }
            }
        } else {
            Ti.API.info("Nessuna foto scelta");
            if ("" != $.W.value) {
                usr.set("cit", $.W.value);
                usr.save();
            }
            if (0 != typebanner) {
                usr.set("banner", typebanner);
                usr.save();
            }
        }
        $.c.close();
        Alloy.createController("index").getView().open();
    }
    function changephoto() {
        Titanium.Media.openPhotoGallery({
            success: function(e) {
                var version = Ti.Platform.getVersion();
                var ver = parseInt(version);
                Ti.API.info("os version" + version);
                if (ver > 5) {
                    Ti.API.info("versione>5");
                    if (true == Ti.Filesystem.hasStoragePermissions()) {
                        Ti.API.info("Ho i permessi");
                        var blob = e.media;
                        imgAux = blob.imageAsThumbnail(120, 0, 30);
                        imgSup = blob.imageAsThumbnail(120, 0, 30);
                        $.f.image = blob.imageAsThumbnail(120, 0, 30);
                    } else {
                        Ti.API.info("Non ho i permessi");
                        Ti.Filesystem.requestStoragePermissions(function() {
                            var blob = e.media;
                            imgAux = blob.imageAsThumbnail(120, 0, 30);
                            imgSup = blob.imageAsThumbnail(120, 0, 30);
                            $.f.image = blob.imageAsThumbnail(120, 0, 30);
                        });
                    }
                } else {
                    var blob = e.media;
                    imgAux = blob.imageAsThumbnail(120, 0, 30);
                    $.f.image = blob.imageAsThumbnail(120, 0, 30);
                }
            },
            cancel: function() {
                alert("errore: nessuna immagine selezionata");
            },
            allowEdition: true
        });
    }
    function saveImm() {
        var filename = "photoprofile.png";
        var parent = Ti.Filesystem.applicationDataDirectory;
        var file = Ti.Filesystem.getFile(parent, filename);
        Ti.API.info("ho preso l immagine del if" + imgAux);
        var imm = imgAux;
        Ti.Utils.base64encode(imm).toString();
        Ti.API.info("imm" + imm);
        file.write(imm);
        Ti.App.Properties.setString("imagepath", Ti.Filesystem.applicationDataDirectory + filename);
        Ti.API.info("native->" + file.nativePath);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "change_me";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.c = Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        id: "c",
        title: "Modifica il tuo Profilo"
    });
    $.__views.c && $.addTopLevelView($.__views.c);
    $.__views.improf = Ti.UI.createView({
        height: "13%",
        borderColor: "#e6e6e6",
        id: "improf"
    });
    $.__views.c.add($.__views.improf);
    changephoto ? $.addListener($.__views.improf, "click", changephoto) : __defers["$.__views.improf!click!changephoto"] = true;
    $.__views.f = Ti.UI.createImageView({
        right: 5,
        borderRadius: 40,
        height: 50,
        width: 50,
        id: "f",
        image: "/images/check.png"
    });
    $.__views.improf.add($.__views.f);
    $.__views.ff = Ti.UI.createLabel({
        font: {
            fontSize: 15
        },
        color: "black",
        left: 5,
        text: "Immagine Profilo",
        id: "ff"
    });
    $.__views.improf.add($.__views.ff);
    $.__views.citazione = Ti.UI.createView({
        height: "13%",
        borderColor: "#e6e6e6",
        id: "citazione"
    });
    $.__views.c.add($.__views.citazione);
    $.__views.e = Ti.UI.createLabel({
        font: {
            fontSize: 15
        },
        color: "black",
        left: 10,
        text: "Citazione",
        id: "e"
    });
    $.__views.citazione.add($.__views.e);
    $.__views.W = Ti.UI.createTextField({
        color: "black",
        hintText: "scrivi una citazione",
        hintTextColor: "gray",
        id: "W",
        editable: true
    });
    $.__views.citazione.add($.__views.W);
    $.__views.viewbanner = Ti.UI.createView({
        height: "20%",
        borderColor: "#e6e6e6",
        id: "viewbanner"
    });
    $.__views.c.add($.__views.viewbanner);
    $.__views.Vbanner = Ti.UI.createView({
        height: "13%",
        borderColor: "#e6e6e6",
        id: "Vbanner"
    });
    $.__views.c.add($.__views.Vbanner);
    lobanner ? $.addListener($.__views.Vbanner, "click", lobanner) : __defers["$.__views.Vbanner!click!lobanner"] = true;
    $.__views.dddddd = Ti.UI.createLabel({
        font: {
            fontSize: 15
        },
        color: "black",
        left: 10,
        text: "Scegli il tuo genere preferito!",
        id: "dddddd"
    });
    $.__views.Vbanner.add($.__views.dddddd);
    $.__views.banvi = Ti.UI.createView({
        height: Ti.UI.FILL,
        borderColor: "#e6e6e6",
        id: "banvi"
    });
    $.__views.c.add($.__views.banvi);
    $.__views.save_imp = Ti.UI.createImageView({
        bottom: 5,
        right: 10,
        height: 100,
        width: 100,
        id: "save_imp",
        image: "/images/saveimp.png"
    });
    $.__views.banvi.add($.__views.save_imp);
    save ? $.addListener($.__views.save_imp, "click", save) : __defers["$.__views.save_imp!click!save"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var imgSup;
    var imgAux;
    var r = require("parseCall");
    var usr = Parse.User.current();
    var b = usr.get("banner");
    var str = r.banner(b);
    $.viewbanner.setBackgroundImage(str);
    var typebanner = null;
    var lobanner = function() {
        var picker = Ti.UI.createPicker({
            height: "150px",
            width: "70%",
            backgroundColor: "red"
        });
        var data = [];
        data[0] = Ti.UI.createPickerRow({
            title: "Nessuno",
            color: "black"
        });
        data[1] = Ti.UI.createPickerRow({
            title: "Fantascienza",
            color: "black"
        });
        data[2] = Ti.UI.createPickerRow({
            title: "Fantasy",
            color: "black"
        });
        data[3] = Ti.UI.createPickerRow({
            title: "Romantico",
            color: "black"
        });
        data[4] = Ti.UI.createPickerRow({
            title: "Horror",
            color: "black"
        });
        data[5] = Ti.UI.createPickerRow({
            title: "Giallo",
            color: "black"
        });
        picker.add(data);
        picker.selectionIndicator = true;
        $.Vbanner.add(picker);
        $.dddddd.hide();
        picker.addEventListener("change", function() {
            Ti.API.info("_^_^_^_^_^_^_^_^_^_^_^_^_^_");
            var xyz = picker.getSelectedRow(0).title;
            Ti.API.info("e é " + picker.getSelectedRow(0).title);
            if ("Nessuno" == xyz) {
                typebanner = 0;
                $.Vbanner.setBackgroundImage("/images/banner/banner-default.jpg");
            } else {
                if ("Fantasy" == xyz) {
                    typebanner = 2;
                    $.Vbanner.setBackgroundImage("/images/banner/fantasy.jpg");
                }
                if ("Fantascienza" == xyz) {
                    typebanner = 1;
                    $.Vbanner.setBackgroundImage("/images/banner/fantascienza.jpg");
                }
                if ("Horror" == xyz) {
                    typebanner = 4;
                    $.Vbanner.setBackgroundImage("/images/banner/horror.jpg");
                }
                if ("Giallo" == xyz) {
                    typebanner = 3;
                    $.Vbanner.setBackgroundImage("/images/banner/giallo.jpg");
                }
                if ("Romantico" == xyz) {
                    typebanner = 5;
                    $.Vbanner.setBackgroundImage("/images/banner/rosa.jpg");
                }
            }
        });
    };
    this.changeBan = function(e, f, imagepath, cit) {
        $.viewbanner.setBackgroundImage(e);
        typebanner = f;
        $.f.image = imagepath;
        $.W.value = cit;
    };
    __defers["$.__views.improf!click!changephoto"] && $.addListener($.__views.improf, "click", changephoto);
    __defers["$.__views.Vbanner!click!lobanner"] && $.addListener($.__views.Vbanner, "click", lobanner);
    __defers["$.__views.save_imp!click!save"] && $.addListener($.__views.save_imp, "click", save);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;