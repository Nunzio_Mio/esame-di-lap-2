function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function likefunction() {
        var l = $.likeb.getTitle();
        if (0 == l.localeCompare(lb)) {
            var num = parseInt($.mi_piace.getText());
            var inr = num + 1;
            likepost(args.user, args.commenttext);
            var strin = inr + " mi piace";
            $.mi_piace.setText(strin);
            var mess = xnome + " ha messo mi piace al tuo commento su " + args.book;
            var req2 = {
                msg: mess,
                usr: args.user
            };
            Parse.Cloud.run("sendNotifResp", req2);
            $.likeb.setTitle("Non mi piace");
        } else {
            var num = parseInt($.mi_piace.getText());
            var inr = num - 1;
            dislikepost(args.user, args.commenttext);
            var strin = inr + " mi piace";
            $.mi_piace.setText(strin);
            $.likeb.setTitle("Mi piace");
        }
    }
    function CommentFunction() {
        Ti.API.info("entro in funzione");
        var spe = {
            user: args.user,
            comm: args.commenttext,
            libro: args.book
        };
        var c = Alloy.createController("commentView", spe);
        var win = c.getView();
        win.show();
        Ti.API.info("dovrebbe aver clickato");
    }
    function openUser() {
        var usr = Parse.User.current();
        var nome = usr.get("username");
        var n = nome.localeCompare(args.user);
        if (0 == n) Alloy.createController("profView").getView().open(); else {
            us.retriveUserProfle(args.user);
            var a = Alloy.createController("usrpage");
            a.dammiiltitolo(args.user);
            var won = a.getView();
            won.setTitle(args.user);
            won.open();
        }
    }
    function likepost(users, comm) {
        var query = new Parse.Query("commentModel");
        query.equalTo("user", users);
        query.equalTo("commenttext", comm);
        query.find({
            success: function(results) {
                var li = results[0].get("like");
                li.toString();
                var x = parseInt(li) + 1;
                results[0].set("like", x);
                results[0].save();
                var rel = results[0].relation("likeuser");
                rel.add(xuser);
                rel.save();
            },
            error: function(e) {
                Ti.API.info("errore: " + e.errorDescription);
            }
        });
    }
    function dislikepost(users, comm) {
        var query = new Parse.Query("commentModel");
        query.equalTo("user", users);
        query.equalTo("commenttext", comm);
        query.find({
            success: function(results) {
                var li = results[0].get("like");
                li.toString();
                var x = parseInt(li) - 1;
                results[0].set("like", x);
                results[0].save();
                var rel = results[0].relation("likeuser");
                rel.remove(xuser);
                rel.save();
            },
            error: function(e) {
                Ti.API.info("errore: " + e.errorDescription);
            }
        });
    }
    function hoillike(users, comm) {
        var query = new Parse.Query("commentModel");
        query.equalTo("user", users);
        query.equalTo("commenttext", comm);
        query.find({
            success: function(results) {
                var rel = results[0].relation("likeuser");
                var q = rel.query();
                q.find({
                    success: function(res) {
                        for (var i = 0; i < res.length; i++) $.likeb.setTitle(0 == res[i].get("username").localeCompare(xnome) ? "Non mi piace" : "Mi piace");
                    },
                    error: function() {
                        Ti.API.info("--------error di hoillike");
                    }
                });
            },
            error: function(e) {
                Ti.API.info("errore: " + e.errorDescription);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "simpRow";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.d = Ti.UI.createTableViewRow({
        height: Ti.UI.SIZE,
        backgroundColor: "#edf6f7",
        id: "d",
        layout: "vertical"
    });
    $.__views.d && $.addTopLevelView($.__views.d);
    $.__views.__alloyId65 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId65"
    });
    $.__views.d.add($.__views.__alloyId65);
    $.__views.usr = Ti.UI.createLabel({
        id: "usr",
        color: "black",
        top: "5px"
    });
    $.__views.__alloyId65.add($.__views.usr);
    openUser ? $.addListener($.__views.usr, "click", openUser) : __defers["$.__views.usr!click!openUser"] = true;
    $.__views.textc = Ti.UI.createLabel({
        right: "40px",
        left: "40px",
        id: "textc",
        color: "black",
        top: "50px"
    });
    $.__views.__alloyId65.add($.__views.textc);
    $.__views.bookL = Ti.UI.createLabel({
        id: "bookL",
        color: "black",
        top: "30px",
        left: "10px",
        right: "10px"
    });
    $.__views.__alloyId65.add($.__views.bookL);
    $.__views.__alloyId66 = Ti.UI.createLabel({
        height: 2,
        width: "94%",
        top: "10px",
        backgroundColor: "#22728f",
        id: "__alloyId66"
    });
    $.__views.__alloyId65.add($.__views.__alloyId66);
    $.__views.mi_piace = Ti.UI.createLabel({
        id: "mi_piace",
        left: 10,
        top: 20,
        color: "#828282"
    });
    $.__views.__alloyId65.add($.__views.mi_piace);
    $.__views.risposta = Ti.UI.createLabel({
        text: "Risposte",
        id: "risposta",
        left: 10,
        top: 7,
        color: "#828282"
    });
    $.__views.__alloyId65.add($.__views.risposta);
    $.__views.likeb = Ti.UI.createButton({
        backgroundColor: "#22728f",
        color: "white",
        title: "Mi piace",
        id: "likeb",
        right: 0,
        width: "100%"
    });
    $.__views.__alloyId65.add($.__views.likeb);
    likefunction ? $.addListener($.__views.likeb, "click", likefunction) : __defers["$.__views.likeb!click!likefunction"] = true;
    $.__views.commb = Ti.UI.createButton({
        backgroundColor: "#001f2d",
        color: "white",
        title: "Commenta",
        id: "commb",
        right: 0,
        width: "100%"
    });
    $.__views.__alloyId65.add($.__views.commb);
    CommentFunction ? $.addListener($.__views.commb, "click", CommentFunction) : __defers["$.__views.commb!click!CommentFunction"] = true;
    $.__views.__alloyId67 = Ti.UI.createLabel({
        height: "50px",
        width: "100%",
        backgroundColor: "#b9b9ac",
        id: "__alloyId67"
    });
    $.__views.__alloyId65.add($.__views.__alloyId67);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var us = require("parseCall");
    Ti.API.info("user row->" + args.user);
    $.usr.text = args.user;
    Ti.API.info("Text row->" + args.commenttext);
    $.textc.text = "'' " + args.commenttext + " ''";
    Ti.API.info("Book Row->" + args.book);
    $.bookL.text = "su " + args.book;
    Ti.API.info("Book Row->" + args.like);
    $.mi_piace.text = args.like + " mi piace";
    var xuser = Parse.User.current();
    var xnome = xuser.get("username");
    hoillike(args.user, args.commenttext);
    var lb = "Mi piace";
    __defers["$.__views.usr!click!openUser"] && $.addListener($.__views.usr, "click", openUser);
    __defers["$.__views.likeb!click!likefunction"] && $.addListener($.__views.likeb, "click", likefunction);
    __defers["$.__views.commb!click!CommentFunction"] && $.addListener($.__views.commb, "click", CommentFunction);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;