function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId51() {
        $.__views.win.removeEventListener("open", __alloyId51);
        if ($.__views.win.activity) $.__views.win.activity.onCreateOptionsMenu = function(e) {
            $.__views.txt = Ti.UI.createTextField({
                width: "70%",
                color: "white",
                hintText: "Cerca il tuo libro                           ",
                hintTextColor: "gray",
                returnKeyType: Ti.UI.RETURNKEY_SEARCH,
                id: "txt"
            });
            serc ? $.addListener($.__views.txt, "return", serc) : __defers["$.__views.txt!return!serc"] = true;
            $.txt = $.__views.txt;
            var __alloyId48 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                id: "__alloyId47"
            };
            $.__views.txt && (__alloyId48.actionView = $.__views.txt);
            $.__views.__alloyId47 = e.menu.add(_.pick(__alloyId48, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId47.applyProperties(_.omit(__alloyId48, Alloy.Android.menuItemCreateArgs));
            $.__alloyId47 = $.__views.__alloyId47;
            var __alloyId50 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                icon: "/s1.png",
                id: "__alloyId49"
            };
            $.__views.__alloyId49 = e.menu.add(_.pick(__alloyId50, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId49.applyProperties(_.omit(__alloyId50, Alloy.Android.menuItemCreateArgs));
            $.__alloyId49 = $.__views.__alloyId49;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "searchLibri";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        title: "",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.wait = Alloy.createController("waitView", {
        id: "wait",
        __parentSymbol: $.__views.win
    });
    $.__views.wait.setParent($.__views.win);
    $.__views.win.addEventListener("open", __alloyId51);
    $.__views.v1 = Ti.UI.createView({
        height: Ti.UI.FILL,
        id: "v1"
    });
    $.__views.win.add($.__views.v1);
    $.__views.books = Ti.UI.createTableView({
        zIndex: 1,
        id: "books"
    });
    $.__views.v1.add($.__views.books);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var data = [];
    var a1 = [];
    var info = [];
    var pRequest = require("parseCall");
    var serc = function() {
        $.txt.blur();
        $.wait.getView().height = Ti.UI.FILL;
        $.wait.getView().width = Ti.UI.FILL;
        $.wait.getView().show();
        var titleSearch = $.txt.value.replace(" ", "+");
        var search = "https://www.googleapis.com/books/v1/volumes?q=" + titleSearch + "&orderBy=relevance";
        var web = Titanium.Network.createHTTPClient();
        web.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36");
        web.open("GET", search);
        web.onload = function() {
            Ti.API.info("data json: " + this.responseText);
            data = JSON.parse(this.responseText);
            if (data["totalItems"] > 0) for (var i = 0; 10 > i; i++) {
                try {
                    var cover = data["items"][i]["volumeInfo"]["imageLinks"].smallThumbnail;
                } catch (e) {
                    var cover = "/images/notfound.png";
                }
                try {
                    var titolo = data["items"][i]["volumeInfo"].title;
                } catch (e) {
                    var titolo = "no Title";
                }
                if ("object" == typeof data["items"][i]["volumeInfo"].authors) var autore = data["items"][i]["volumeInfo"].authors[0]; else var autore = data["items"][i]["volumeInfo"].authors;
                try {
                    var desc = data["items"][i]["volumeInfo"].description;
                } catch (e) {
                    var desc = "Non Disponibile";
                }
                try {
                    var isbn13 = data["items"][i]["volumeInfo"].industryIdentifiers[0].identifier;
                } catch (e) {
                    var isbn13 = -1;
                }
                try {
                    var isbn10 = data["items"][i]["volumeInfo"].industryIdentifiers[1].identifier;
                } catch (e) {
                    var isbn10 = -1;
                }
                var row = Ti.UI.createTableViewRow();
                info[i] = {
                    titolo: titolo,
                    autore: autore,
                    copertina: cover,
                    descrizione: desc,
                    isbn13: isbn13,
                    isbn10: isbn10
                };
                var v1 = Ti.UI.createView();
                v1.borderColor = "#e6e6e6";
                if (titolo.length > 24) {
                    var t = titolo.slice(0, 23);
                    var newtit = t + "...";
                } else var newtit = titolo;
                var t2 = Ti.UI.createLabel({
                    text: autore,
                    color: "black",
                    left: "120px",
                    top: 30
                });
                var t1 = Ti.UI.createLabel({
                    text: newtit,
                    color: "black",
                    left: "120px",
                    top: 10
                });
                var c1 = Ti.UI.createImageView({
                    height: "150px",
                    width: "100px",
                    image: cover,
                    left: 5
                });
                var plus = Ti.UI.createImageView({
                    image: "/images/plus.png",
                    right: 7,
                    height: "90px",
                    width: "90px"
                });
                v1.add(plus);
                v1.add(t1);
                v1.add(t2);
                v1.add(c1);
                row.add(v1);
                plus.idrow = i;
                plus.addEventListener("click", function(e) {
                    var book = {
                        title: info[e.source.idrow].titolo,
                        authors: info[e.source.idrow].autore,
                        isbn: info[e.source.idrow].isbn10,
                        isbn13: info[e.source.idrow].isbn13,
                        desc: info[e.source.idrow].descrizione,
                        imageName: info[e.source.idrow].copertina
                    };
                    pRequest.newFuncAdd(info[e.source.idrow].titolo, book, Parse.User.current());
                    e.source.image = "/images/check.png";
                });
                a1[i] = row;
            } else {
                var failLabel = Ti.UI.createLabel({
                    color: "black",
                    text: "non ho trovato nulla"
                });
                $.books.add(failLabel);
            }
            $.books.setData(a1);
            $.wait.getView().hide();
            $.wait.getView().height = 0;
            $.wait.getView().width = 0;
        };
        web.error = function() {
            Ti.API.info("Mi dispiace ma non troviamo il tuo libro :(");
            alert("Mi dispiace ma non troviamo il tuo libro :(");
        };
        web.send();
    };
    $.books.addEventListener("longclick", function(e) {
        Ti.API.info(e.index);
        Ti.API.info(info[e.index].titolo);
        Ti.API.info(info[e.index].autore);
        Ti.API.info(info[e.index].copertina);
        Ti.API.info(info[e.index].descrizione);
        var c = Alloy.createController("bookpage");
        c.gettitolo(info[e.index].titolo);
        c.getscrittore(info[e.index].autore);
        c.getlibro(info[e.index].copertina);
        c.getdescrizione(info[e.index].descrizione);
        c.getisbn13(info[e.index].isbn13);
        c.getisbn10(info[e.index].isbn10);
        var window = c.getView();
        window.title = info[e.index].titolo;
        window.open();
    });
    __defers["$.__views.txt!return!serc"] && $.addListener($.__views.txt, "return", serc);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;