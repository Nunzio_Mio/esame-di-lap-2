function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function __alloyId22() {
        $.__views.bckgnd.removeEventListener("open", __alloyId22);
        if ($.__views.bckgnd.activity) $.__views.bckgnd.activity.onCreateOptionsMenu = function(e) {
            var __alloyId16 = {
                title: "Modifica profilo",
                showAsAction: Ti.Android.SHOW_AS_ACTION_NEVER,
                id: "__alloyId15"
            };
            $.__views.__alloyId15 = e.menu.add(_.pick(__alloyId16, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId15.applyProperties(_.omit(__alloyId16, Alloy.Android.menuItemCreateArgs));
            $.__alloyId15 = $.__views.__alloyId15;
            Cambiaprof ? $.addListener($.__views.__alloyId15, "click", Cambiaprof) : __defers["$.__views.__alloyId15!click!Cambiaprof"] = true;
            var __alloyId17 = {
                id: "sett",
                title: "Logout",
                showAsAction: Ti.Android.SHOW_AS_ACTION_NEVER
            };
            $.__views.sett = e.menu.add(_.pick(__alloyId17, Alloy.Android.menuItemCreateArgs));
            $.__views.sett.applyProperties(_.omit(__alloyId17, Alloy.Android.menuItemCreateArgs));
            $.sett = $.__views.sett;
            Impostazioni ? $.addListener($.__views.sett, "click", Impostazioni) : __defers["$.__views.sett!click!Impostazioni"] = true;
            var __alloyId19 = {
                title: "About us",
                showAsAction: Ti.Android.SHOW_AS_ACTION_NEVER,
                id: "__alloyId18"
            };
            $.__views.__alloyId18 = e.menu.add(_.pick(__alloyId19, Alloy.Android.menuItemCreateArgs));
            $.__views.__alloyId18.applyProperties(_.omit(__alloyId19, Alloy.Android.menuItemCreateArgs));
            $.__alloyId18 = $.__views.__alloyId18;
            Aboutus ? $.addListener($.__views.__alloyId18, "click", Aboutus) : __defers["$.__views.__alloyId18!click!Aboutus"] = true;
            var __alloyId20 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                id: "search",
                icon: "/s1.png"
            };
            $.__views.search = e.menu.add(_.pick(__alloyId20, Alloy.Android.menuItemCreateArgs));
            $.__views.search.applyProperties(_.omit(__alloyId20, Alloy.Android.menuItemCreateArgs));
            $.search = $.__views.search;
            laSearch ? $.addListener($.__views.search, "click", laSearch) : __defers["$.__views.search!click!laSearch"] = true;
            var __alloyId21 = {
                showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
                id: "notification",
                icon: "/s2.png"
            };
            $.__views.notification = e.menu.add(_.pick(__alloyId21, Alloy.Android.menuItemCreateArgs));
            $.__views.notification.applyProperties(_.omit(__alloyId21, Alloy.Android.menuItemCreateArgs));
            $.notification = $.__views.notification;
            showsidebar ? $.addListener($.__views.notification, "click", showsidebar) : __defers["$.__views.notification!click!showsidebar"] = true;
            $.__views.bckgnd.activity.actionBar;
        }; else {
            Ti.API.warn("You attempted to attach an Android Menu to a lightweight Window");
            Ti.API.warn("or other UI component which does not have an Android activity.");
            Ti.API.warn("Android Menus can only be opened on TabGroups and heavyweight Windows.");
        }
    }
    function __alloyId23() {
        $.__views.bckgnd.removeEventListener("open", __alloyId23);
        if ($.__views.bckgnd.activity) $.__views.bckgnd.activity.actionBar.title = "Da Leggere"; else {
            Ti.API.warn("You attempted to access an Activity on a lightweight Window or other");
            Ti.API.warn("UI component which does not have an Android activity. Android Activities");
            Ti.API.warn("are valid with only windows in TabGroups or heavyweight Windows.");
        }
    }
    function Cambiaprof() {
        Alloy.createController("change_me").getView().open();
    }
    function Impostazioni() {
        var currentUser = Parse.User.current();
        if (currentUser) {
            alert("Arrivederci :-)");
            Parse.User.logOut();
            Alloy.createController("logView").getView().open();
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.bckgnd = Ti.UI.createWindow({
        backgroundColor: "#224d72",
        id: "bckgnd"
    });
    $.__views.bckgnd && $.addTopLevelView($.__views.bckgnd);
    $.__views.bckgnd.addEventListener("open", __alloyId22);
    $.__views.bckgnd.addEventListener("open", __alloyId23);
    $.__views.notif = Alloy.createController("notifView", {
        id: "notif",
        __parentSymbol: $.__views.bckgnd
    });
    $.__views.notif.setParent($.__views.bckgnd);
    var __alloyId24 = [];
    $.__views.libView = Alloy.createController("libView", {
        id: "libView"
    });
    __alloyId24.push($.__views.libView.getViewEx({
        recurse: true
    }));
    $.__views.simposio = Alloy.createController("simposio", {
        id: "simposio"
    });
    __alloyId24.push($.__views.simposio.getViewEx({
        recurse: true
    }));
    $.__views.profilo = Alloy.createController("profView", {
        id: "profilo"
    });
    __alloyId24.push($.__views.profilo.getViewEx({
        recurse: true
    }));
    $.__views.principal = Ti.UI.createScrollableView({
        backgroundColor: "#224d72",
        top: 0,
        height: "90%",
        views: __alloyId24,
        id: "principal"
    });
    $.__views.bckgnd.add($.__views.principal);
    changeTitleBar ? $.addListener($.__views.principal, "scrollEnd", changeTitleBar) : __defers["$.__views.principal!scrollEnd!changeTitleBar"] = true;
    $.__views.btnv = Ti.UI.createView({
        backgroundColor: "#224d72",
        bottom: 0,
        height: "10%",
        id: "btnv"
    });
    $.__views.bckgnd.add($.__views.btnv);
    $.__views.libBtn = Ti.UI.createButton({
        backgroundColor: "transparent",
        left: 2,
        id: "libBtn",
        image: "/r1.png"
    });
    $.__views.btnv.add($.__views.libBtn);
    canView ? $.addListener($.__views.libBtn, "click", canView) : __defers["$.__views.libBtn!click!canView"] = true;
    $.__views.simpBtn = Ti.UI.createButton({
        backgroundColor: "transparent",
        id: "simpBtn",
        image: "/r2.png"
    });
    $.__views.btnv.add($.__views.simpBtn);
    canView ? $.addListener($.__views.simpBtn, "click", canView) : __defers["$.__views.simpBtn!click!canView"] = true;
    $.__views.profBtn = Ti.UI.createButton({
        backgroundColor: "transparent",
        right: 2,
        id: "profBtn",
        image: "/r3.png"
    });
    $.__views.btnv.add($.__views.profBtn);
    canView ? $.addListener($.__views.profBtn, "click", canView) : __defers["$.__views.profBtn!click!canView"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var parCall = require("parseCall");
    var initApp = function() {
        Ti.API.info("Ora parese");
        var currentUser = Parse.User.current();
        if (currentUser) {
            Ti.API.info("Loogato");
            parCall.toParseInstall(Ti.App.Properties.getString("deviceToken"), "android", Parse.User.current().getUsername());
            $.bckgnd.open();
        } else {
            Ti.API.info("in log");
            Alloy.createController("logView").getView().open();
        }
        Ti.API.info("Finish");
    };
    if (Ti.Network.online) {
        parCall.view = $.notif;
        Ti.API.info("online");
        Alloy.Globals.useCloud = true;
        var hideSide = true;
        parCall.toGoogleReg();
        Ti.API.info("richiamo initApp");
        initApp();
    } else {
        Ti.API.info("Offline");
        {
            Alloy.createController("Offline").getView().open();
        }
        $.bckgnd.close();
    }
    var showsidebar = function() {
        if (hideSide) {
            var anim = Ti.UI.createAnimation({
                left: 600,
                duration: 3e3
            });
            $.notif.getView().show(anim);
            hideSide = false;
        } else {
            $.notif.getView().hide();
            hideSide = true;
        }
    };
    var Aboutus = function() {
        Alloy.createController("aboutas").getView().open();
    };
    var laSearch = function() {
        Alloy.createController("searchView").getView().open();
    };
    var canView = function(e) {
        $.principal.scrollToView(e.source.id == $.libBtn.id ? 0 : e.source.id == $.simpBtn.id ? 1 : 2);
    };
    var changeTitleBar = function() {
        Ti.API.info("entro nel cambio con ->" + $.principal.getCurrentPage());
        $.bckgnd.getActivity().actionBar.title = 0 == $.principal.getCurrentPage() ? "Da Leggere" : 1 == $.principal.getCurrentPage() ? "Simposio" : "Profilo";
        if (!Ti.Network.online) {
            Ti.API.info("Offline");
            {
                Alloy.createController("Offline").getView().open();
            }
            $.bckgnd.close();
        }
    };
    __defers["$.__views.__alloyId15!click!Cambiaprof"] && $.addListener($.__views.__alloyId15, "click", Cambiaprof);
    __defers["$.__views.sett!click!Impostazioni"] && $.addListener($.__views.sett, "click", Impostazioni);
    __defers["$.__views.__alloyId18!click!Aboutus"] && $.addListener($.__views.__alloyId18, "click", Aboutus);
    __defers["$.__views.search!click!laSearch"] && $.addListener($.__views.search, "click", laSearch);
    __defers["$.__views.notification!click!showsidebar"] && $.addListener($.__views.notification, "click", showsidebar);
    __defers["$.__views.principal!scrollEnd!changeTitleBar"] && $.addListener($.__views.principal, "scrollEnd", changeTitleBar);
    __defers["$.__views.libBtn!click!canView"] && $.addListener($.__views.libBtn, "click", canView);
    __defers["$.__views.simpBtn!click!canView"] && $.addListener($.__views.simpBtn, "click", canView);
    __defers["$.__views.profBtn!click!canView"] && $.addListener($.__views.profBtn, "click", canView);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;