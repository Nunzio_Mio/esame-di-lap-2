function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function bye() {
        Alloy.createController("index").getView().open();
    }
    function submit() {
        var comment = Parse.Object.extend("commentModel");
        var actCom = new comment();
        var usr = Parse.User.current().getUsername();
        var strCm = $.Area.value;
        var id = idMy;
        actCom.save({
            user: usr,
            book: iltitolo,
            commenttext: strCm,
            like: 0
        }, {
            success: function() {
                var rat = $.valuta.value;
                Ti.API.info("il primo rat->" + rat);
                pRequest.addratingMy(id, rat);
                var req = {
                    msg: iltitolo
                };
                Parse.Cloud.run("sendAct", req);
                alert("Commentato");
            },
            error: function() {
                Ti.API.info("Comment no riuscito");
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "comment_page";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.comment_page = Ti.UI.createWindow({
        layout: "vertical",
        title: "Dicci cosa ne pensi",
        backgroundColor: "#f2f2f2",
        id: "comment_page"
    });
    $.__views.comment_page && $.addTopLevelView($.__views.comment_page);
    $.__views.__alloyId10 = Ti.UI.createView({
        height: "20%",
        id: "__alloyId10"
    });
    $.__views.comment_page.add($.__views.__alloyId10);
    $.__views.__alloyId11 = Ti.UI.createLabel({
        color: "black",
        text: "Che valutazione dai?(Da 0 a 5)",
        top: 5,
        id: "__alloyId11"
    });
    $.__views.__alloyId10.add($.__views.__alloyId11);
    $.__views.valuta = Ti.UI.createTextField({
        hintText: "Metti il tuo voto",
        hintTextColor: "gray",
        bottom: 5,
        color: "black",
        id: "valuta"
    });
    $.__views.__alloyId10.add($.__views.valuta);
    $.__views.__alloyId12 = Ti.UI.createView({
        height: "20%",
        id: "__alloyId12"
    });
    $.__views.comment_page.add($.__views.__alloyId12);
    $.__views.__alloyId13 = Ti.UI.createLabel({
        color: "black",
        text: "Lascia un commento",
        top: 5,
        id: "__alloyId13"
    });
    $.__views.__alloyId12.add($.__views.__alloyId13);
    $.__views.Area = Ti.UI.createTextArea({
        height: "70%",
        color: "black",
        id: "Area",
        editable: true
    });
    $.__views.__alloyId12.add($.__views.Area);
    $.__views.__alloyId14 = Ti.UI.createView({
        height: "20%",
        id: "__alloyId14"
    });
    $.__views.comment_page.add($.__views.__alloyId14);
    $.__views.leave = Ti.UI.createButton({
        bottom: 5,
        left: 10,
        backgroundColor: "#224d72",
        borderRadius: 40,
        title: "No",
        id: "leave"
    });
    $.__views.__alloyId14.add($.__views.leave);
    bye ? $.addListener($.__views.leave, "click", bye) : __defers["$.__views.leave!click!bye"] = true;
    $.__views.submit = Ti.UI.createButton({
        bottom: 5,
        right: 10,
        backgroundColor: "#224d72",
        borderRadius: 40,
        title: "Commenta",
        id: "submit"
    });
    $.__views.__alloyId14.add($.__views.submit);
    submit ? $.addListener($.__views.submit, "click", submit) : __defers["$.__views.submit!click!submit"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var pRequest = require("parseCall");
    var iltitolo;
    var idMy;
    this.gettitolo = function(e) {
        iltitolo = e;
    };
    this.getidMy = function(e) {
        idMy = e;
    };
    __defers["$.__views.leave!click!bye"] && $.addListener($.__views.leave, "click", bye);
    __defers["$.__views.submit!click!submit"] && $.addListener($.__views.submit, "click", submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;