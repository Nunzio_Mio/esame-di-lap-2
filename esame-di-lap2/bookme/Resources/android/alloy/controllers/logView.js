function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "logView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.lawin = Ti.UI.createWindow({
        backgroundColor: "#224d72",
        layout: "vertical",
        id: "lawin"
    });
    $.__views.lawin && $.addTopLevelView($.__views.lawin);
    $.__views.lor = Ti.UI.createView({
        backgroundColor: "transparent",
        height: "5%",
        width: "70%",
        borderColor: "transparent",
        borderWidth: 2,
        borderRadius: 20,
        font: {
            fontSize: 20
        },
        id: "lor"
    });
    $.__views.lawin.add($.__views.lor);
    $.__views.__alloyId34 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#ffffff",
        text: "Login",
        id: "__alloyId34"
    });
    $.__views.lor.add($.__views.__alloyId34);
    $.__views.lgn = Ti.UI.createView({
        backgroundColor: "white",
        height: "30%",
        width: "70%",
        borderColor: "#9cc2e2",
        borderWidth: 2,
        borderRadius: 20,
        id: "lgn",
        layout: "vertical"
    });
    $.__views.lawin.add($.__views.lgn);
    $.__views.useremail = Ti.UI.createTextField({
        hintTextColor: "gray",
        color: "black",
        hintText: "Il tuo nickname",
        id: "useremail"
    });
    $.__views.lgn.add($.__views.useremail);
    $.__views.userpass = Ti.UI.createTextField({
        hintTextColor: "gray",
        color: "black",
        hintText: "La tua Password",
        passwordMask: true,
        id: "userpass"
    });
    $.__views.lgn.add($.__views.userpass);
    $.__views.loginbtt = Ti.UI.createButton({
        borderRadius: 15,
        backgroundColor: "#224d72",
        title: "Login",
        id: "loginbtt"
    });
    $.__views.lgn.add($.__views.loginbtt);
    log ? $.addListener($.__views.loginbtt, "click", log) : __defers["$.__views.loginbtt!click!log"] = true;
    $.__views.or = Ti.UI.createView({
        backgroundColor: "transparent",
        height: "5%",
        width: "70%",
        borderColor: "transparent",
        borderWidth: 2,
        borderRadius: 20,
        id: "or"
    });
    $.__views.lawin.add($.__views.or);
    $.__views.__alloyId35 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#ffffff",
        text: "or",
        id: "__alloyId35"
    });
    $.__views.or.add($.__views.__alloyId35);
    $.__views.lor = Ti.UI.createView({
        backgroundColor: "transparent",
        height: "5%",
        width: "70%",
        borderColor: "transparent",
        borderWidth: 2,
        borderRadius: 20,
        font: {
            fontSize: 20
        },
        id: "lor"
    });
    $.__views.lawin.add($.__views.lor);
    $.__views.__alloyId36 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#ffffff",
        text: "SignUp",
        id: "__alloyId36"
    });
    $.__views.lor.add($.__views.__alloyId36);
    $.__views.rgstr = Ti.UI.createView({
        backgroundColor: "white",
        height: "40%",
        width: "70%",
        borderColor: "#9cc2e2",
        borderWidth: 2,
        borderRadius: 20,
        id: "rgstr",
        layout: "vertical"
    });
    $.__views.lawin.add($.__views.rgstr);
    $.__views.usernick = Ti.UI.createTextField({
        hintTextColor: "gray",
        color: "black",
        hintText: "Inserisci il tuo Nickname",
        id: "usernick"
    });
    $.__views.rgstr.add($.__views.usernick);
    $.__views.usremail = Ti.UI.createTextField({
        hintTextColor: "gray",
        color: "black",
        hintText: "Inserisci la tua Email",
        id: "usremail"
    });
    $.__views.rgstr.add($.__views.usremail);
    $.__views.usrpass = Ti.UI.createTextField({
        hintTextColor: "gray",
        color: "black",
        hintText: "Inserisci la tua Password",
        passwordMask: true,
        id: "usrpass"
    });
    $.__views.rgstr.add($.__views.usrpass);
    $.__views.regbtt = Ti.UI.createButton({
        borderRadius: 15,
        backgroundColor: "#224d72",
        title: "Registrati",
        id: "regbtt"
    });
    $.__views.rgstr.add($.__views.regbtt);
    sig ? $.addListener($.__views.regbtt, "click", sig) : __defers["$.__views.regbtt!click!sig"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var lg = require("parseCall");
    var log = function() {
        lg.logIn($.useremail.value, $.userpass.value);
        var currentUser = Parse.User.current();
        if (currentUser) Alloy.createController("index").getView().open(); else {
            var noLog = Ti.UI.createAlertDialog({
                buttonNames: [ "Ok" ],
                backgroundColor: "#1f1f14",
                height: "50px",
                Ok: 0,
                title: "Login fallito",
                message: "Combinazione nome utente/password errata !"
            });
            noLog.show();
            noLog.addEventListener("click", function(e) {
                e.index === e.source.Ok && noLog.hide();
            });
        }
    };
    var sig = function() {
        lg.signUp($.usernick.value, $.usrpass.value, $.usremail.value);
        var currentUser = Parse.User.current();
        if (currentUser) Alloy.createController("index").getView().open(); else {
            var noLog = Ti.UI.createAlertDialog({
                buttonNames: [ "Ok" ],
                backgroundColor: "#1f1f14",
                height: "50px",
                Ok: 0,
                title: "Nome utente non valido",
                message: "Questo nome utente gia' esiste !"
            });
            noLog.show();
            noLog.addEventListener("click", function(e) {
                e.index === e.source.Ok && noLog.hide();
            });
        }
    };
    __defers["$.__views.loginbtt!click!log"] && $.addListener($.__views.loginbtt, "click", log);
    __defers["$.__views.regbtt!click!sig"] && $.addListener($.__views.regbtt, "click", sig);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;