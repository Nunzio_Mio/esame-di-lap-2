function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function riempiF() {
        var rel = Parse.User.current().relation("following");
        var query = rel.query();
        query.find().then(function(results) {
            if (0 == results.length) alert("Non hai nessun following"); else for (var i = 0; i < results.length; i++) {
                var nome = results[i].get("username");
                var banner = results[i].get("banner");
                Ti.API.info("nome e banner : " + nome + " " + banner);
                var b = r.banner(banner);
                var count;
                Ti.API.info("-----------------------------");
                Ti.API.info("nome: " + nome);
                Ti.API.info("banner: " + b);
                var rel = results[i].relation("myBooks");
                var q = rel.query();
                q.equalTo("read", true);
                q.count({
                    success: function(letti) {
                        Ti.API.info("HO LETTO: " + letti);
                        count = letti;
                        var row = Ti.UI.createTableViewRow();
                        var v1 = Ti.UI.createView();
                        v1.borderColor = "gray";
                        v1.height = "150px";
                        var t1 = Ti.UI.createLabel({
                            text: nome,
                            color: "black",
                            top: 10
                        });
                        Ti.API.info("count: " + count);
                        var t2 = Ti.UI.createLabel({
                            text: "Ha letto: " + count + " libri!",
                            color: "gray",
                            bottom: 10
                        });
                        var c1 = Ti.UI.createImageView({
                            height: "100px",
                            width: "100px",
                            borderRadius: 40,
                            image: b,
                            left: 5
                        });
                        v1.add(c1);
                        v1.add(t1);
                        v1.add(t2);
                        row.add(v1);
                        row.addEventListener("click", function() {
                            var a = Alloy.createController("usrpage");
                            a.dammiiltitolo(nome);
                            var won = a.getView();
                            won.setTitle(nome);
                            won.open();
                        });
                        Ti.API.info("addirittura ho il cazzo duro e arrivo qua!");
                        data[i] = row;
                        $.lista.setData(data);
                        Ti.API.info("datarow");
                    },
                    error: function(error) {
                        Ti.API.info(error.errorState);
                    }
                });
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "following";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.following = Ti.UI.createWindow({
        title: "Following",
        id: "following"
    });
    $.__views.following && $.addTopLevelView($.__views.following);
    $.__views.lista = Ti.UI.createTableView({
        backgroundColor: "white",
        id: "lista"
    });
    $.__views.following.add($.__views.lista);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    data = [];
    var r = require("parseCall");
    riempiF();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;