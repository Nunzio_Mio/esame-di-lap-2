function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function loadingAnimation() {
        $.wImg.image = "/images/gif/gif-" + loaderIndex + ".png";
        loaderIndex++;
        9 === loaderIndex && (loaderIndex = 1);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "waitView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.con = Ti.UI.createView({
        backgroundColor: "#60b6ff",
        opacity: .5,
        id: "con",
        visible: false
    });
    $.__views.con && $.addTopLevelView($.__views.con);
    $.__views.wImg = Ti.UI.createImageView({
        height: "25%",
        width: "25%",
        id: "wImg"
    });
    $.__views.con.add($.__views.wImg);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    Ti.API.info("Wait.....");
    var loaderIndex = 1;
    setInterval(loadingAnimation, 80);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;