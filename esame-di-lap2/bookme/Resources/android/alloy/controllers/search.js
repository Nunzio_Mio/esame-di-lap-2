function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function boh() {
        Ti.App.fireEvent("raimondo");
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "search";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        var __parentSymbol = __processArg(arguments[0], "__parentSymbol");
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    var __alloyId41 = [];
    $.__views.__alloyId43 = Alloy.createController("searchLibri", {
        id: "__alloyId43",
        __parentSymbol: __parentSymbol
    });
    $.__views.__alloyId42 = Ti.UI.createTab({
        window: $.__views.__alloyId43.getViewEx({
            recurse: true
        }),
        title: "Libri",
        id: "__alloyId42"
    });
    __alloyId41.push($.__views.__alloyId42);
    $.__views.__alloyId45 = Alloy.createController("Offline", {
        id: "__alloyId45",
        __parentSymbol: __parentSymbol
    });
    $.__views.__alloyId44 = Ti.UI.createTab({
        window: $.__views.__alloyId45.getViewEx({
            recurse: true
        }),
        title: "Utenti",
        id: "__alloyId44"
    });
    __alloyId41.push($.__views.__alloyId44);
    $.__views.win = Ti.UI.createTabGroup({
        backgroundColor: "white",
        tabs: __alloyId41,
        title: "",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    boh ? $.addListener($.__views.win, "androidback", boh) : __defers["$.__views.win!androidback!boh"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    __defers["$.__views.win!androidback!boh"] && $.addListener($.__views.win, "androidback", boh);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;