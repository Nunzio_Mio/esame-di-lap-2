function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "libView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.w0 = Ti.UI.createWindow({
        id: "w0"
    });
    $.__views.w0 && $.addTopLevelView($.__views.w0);
    $.__views.l0 = Ti.UI.createLabel({
        text: "Non ci sono libri da leggere premi il tasto cerca per aggiungere un libro... Buona lettura !",
        id: "l0",
        top: "50px",
        right: "50px",
        left: "50px",
        zIndex: 999,
        color: "black"
    });
    $.__views.w0.add($.__views.l0);
    $.__views.books = Ti.UI.createTableView({
        backgroundColor: "white",
        id: "books"
    });
    $.__views.w0.add($.__views.books);
    $.__views.wait = Alloy.createController("waitView", {
        id: "wait",
        __parentSymbol: $.__views.w0
    });
    $.__views.wait.setParent($.__views.w0);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    require("parseCall");
    var data = [];
    Ti.App.addEventListener("raimondo", function() {
        $.books.setData([]);
        fillBooks();
    });
    var fillBooks = function() {
        $.wait.getView().height = Ti.UI.FILL;
        $.wait.getView().width = Ti.UI.FILL;
        $.wait.getView().show();
        Ti.API.info("Parto retrive");
        var rel = Parse.User.current().relation("myBooks");
        var query = rel.query();
        query.include("book");
        query.find().then(function(results) {
            if (0 == results.length) {
                $.wait.getView().hide();
                $.wait.getView().height = 0;
                $.wait.getView().width = 0;
            } else {
                for (var i = 0; i < results.length; i++) {
                    $.w0.remove($.l0);
                    if (false == results[i].get("read")) {
                        Ti.API.info("ciclo");
                        var idf = results[i].id;
                        Ti.API.info("id->" + results[i].id);
                        var title = results[i].get("book").get("title");
                        Ti.API.info("titolo->" + results[i].get("book").get("title"));
                        var aut = results[i].get("book").get("authors");
                        Ti.API.info("aut->" + results[i].get("book").get("authors"));
                        var img = results[i].get("book").get("imageName");
                        Ti.API.info("img->" + results[i].get("book").get("imageName"));
                        {
                            results[i].get("read");
                        }
                        Ti.API.info("read->" + results[i].get("read"));
                        {
                            results[i].get("favorite");
                        }
                        Ti.API.info("fav->" + results[i].get("favorite"));
                        var isbn10 = results[i].get("book").get("isbn");
                        Ti.API.info("---------------------------------------");
                        Ti.API.info("isbn in nujmero 10----> " + isbn10);
                        Ti.API.info("-----------------------------------------");
                        Ti.API.info("sto creando la tabview");
                        var row = Ti.UI.createTableViewRow();
                        Ti.API.info("ho creato la tabview");
                        var v1 = Ti.UI.createView();
                        v1.height = "220px";
                        var linea = Ti.UI.createLabel({
                            bottom: 0,
                            backgroundColor: "#e6e6e6",
                            height: "1px",
                            width: "100%"
                        });
                        if (title.length > 24) {
                            var t = title.slice(0, 23);
                            var newtit = t + "...";
                        } else var newtit = title;
                        var t2 = Ti.UI.createLabel({
                            text: aut,
                            color: "black",
                            left: "120px",
                            top: 30
                        });
                        var t1 = Ti.UI.createLabel({
                            text: newtit,
                            color: "black",
                            left: "120px",
                            top: 10
                        });
                        var c1 = Ti.UI.createImageView({
                            height: "150px",
                            width: "100px",
                            image: img,
                            left: 5
                        });
                        var plus = Ti.UI.createImageView({
                            image: "/images/daleggere.png",
                            right: 7,
                            height: "90px",
                            width: "90px"
                        });
                        v1.add(plus);
                        v1.add(t1);
                        v1.add(t2);
                        v1.add(c1);
                        v1.add(linea);
                        row.add(v1);
                        plus.idrow = i;
                        plus.idObject = idf;
                        plus.tL = title;
                        plus.addEventListener("click", function(e) {
                            Ti.API.info("sto eliminando");
                            var index = e.source.idrow;
                            e.source.idObject;
                            $.books.deleteRow(index);
                            Ti.API.info("communico con server");
                            var c = Alloy.createController("leavecomment");
                            c.gettitolo(e.source.tL);
                            c.getidMy(e.source.idObject);
                            var window = c.getView();
                            window.show();
                        });
                        data[i] = row;
                        Alloy.Globals.libri.push(isbn10);
                        Ti.API.info("Alloy globale=> " + Alloy.Globals.libri[i]);
                        Ti.API.info("finito di addare la row");
                    }
                }
                $.wait.getView().hide();
                $.wait.getView().height = 0;
                $.wait.getView().width = 0;
                $.books.setData(data);
            }
        });
    };
    null != Parse.User.current() && fillBooks(data);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;