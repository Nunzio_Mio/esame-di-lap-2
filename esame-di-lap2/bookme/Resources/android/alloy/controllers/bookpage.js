function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "bookpage";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.pagewin = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "#f2f2f2",
        id: "pagewin"
    });
    $.__views.pagewin && $.addTopLevelView($.__views.pagewin);
    $.__views.__alloyId2 = Ti.UI.createScrollView({
        layout: "vertical",
        id: "__alloyId2"
    });
    $.__views.pagewin.add($.__views.__alloyId2);
    $.__views.front = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: "30%",
        id: "front"
    });
    $.__views.__alloyId2.add($.__views.front);
    $.__views.ulibru = Ti.UI.createImageView({
        height: "250px",
        width: "200px",
        left: "20px",
        top: "30px",
        id: "ulibru"
    });
    $.__views.front.add($.__views.ulibru);
    $.__views.utitulu = Ti.UI.createLabel({
        color: "black",
        font: {
            fontSize: 13
        },
        top: "30px",
        left: "240px",
        id: "utitulu"
    });
    $.__views.front.add($.__views.utitulu);
    $.__views.uscrittu = Ti.UI.createLabel({
        color: "black",
        font: {
            fontSize: 13
        },
        top: "100px",
        left: "240px",
        id: "uscrittu"
    });
    $.__views.front.add($.__views.uscrittu);
    $.__views.spazio = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: 40,
        backgroundColor: "#d9d9d9",
        id: "spazio"
    });
    $.__views.__alloyId2.add($.__views.spazio);
    $.__views.description = Ti.UI.createLabel({
        color: "#8c8c8c",
        font: {
            fontSize: 13
        },
        left: 7,
        text: "Descrizione",
        id: "description"
    });
    $.__views.spazio.add($.__views.description);
    $.__views.__alloyId3 = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: Ti.UI.SIZE,
        id: "__alloyId3"
    });
    $.__views.__alloyId2.add($.__views.__alloyId3);
    $.__views.descru = Ti.UI.createLabel({
        color: "black",
        font: {
            fontSize: 13
        },
        id: "descru",
        top: 5,
        bottom: 5,
        left: 5,
        right: 5
    });
    $.__views.__alloyId3.add($.__views.descru);
    $.__views.spazio = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: 40,
        backgroundColor: "#d9d9d9",
        id: "spazio"
    });
    $.__views.__alloyId2.add($.__views.spazio);
    $.__views.description = Ti.UI.createLabel({
        color: "#8c8c8c",
        font: {
            fontSize: 13
        },
        left: 7,
        text: "Voti ricevuti dalla comunità",
        id: "description"
    });
    $.__views.spazio.add($.__views.description);
    $.__views.rati = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: "10%",
        id: "rati"
    });
    $.__views.__alloyId2.add($.__views.rati);
    $.__views.rating = Ti.UI.createLabel({
        color: "black",
        font: {
            fontSize: 13
        },
        text: "0/5",
        id: "rating"
    });
    $.__views.rati.add($.__views.rating);
    $.__views.__alloyId4 = Ti.UI.createView({
        borderColor: "#e6e6e6",
        height: "10%",
        backgroundColor: "#d9d9d9",
        id: "__alloyId4"
    });
    $.__views.__alloyId2.add($.__views.__alloyId4);
    $.__views.addbook = Ti.UI.createButton({
        backgroundColor: "#224d72",
        borderRadius: "40",
        title: "Aggiungi",
        id: "addbook"
    });
    $.__views.__alloyId4.add($.__views.addbook);
    addFun ? $.addListener($.__views.addbook, "click", addFun) : __defers["$.__views.addbook!click!addFun"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var isbn10, isbn13;
    this.gettitolo = function(e) {
        $.utitulu.setText(e);
    };
    this.getscrittore = function(e) {
        $.uscrittu.setText("Autore: " + e);
    };
    this.getlibro = function(e) {
        $.ulibru.setImage(e);
    };
    this.getdescrizione = function(e) {
        $.descru.setText(e);
    };
    this.getisbn10 = function(e) {
        isbn10 = e;
        ratingF();
    };
    this.getisbn13 = function(e) {
        isbn13 = e;
    };
    var addFun = function(title, book, user) {
        var pRequest = require("parseCall");
        pRequest.addParseBookWhitCheck(title, book, user);
    };
    var ratingF = function() {
        Ti.API.info("-------------entro in ratingf-----------");
        var bk = Parse.Object.extend("book");
        var rep_query = new Parse.Query(bk);
        rep_query.equalTo("isbn", isbn10);
        Ti.API.info("-------eseguo query " + isbn10 + "-------------");
        rep_query.find({
            success: function(results) {
                if (0 != results.length) {
                    var intr = results[0].get("rating") + "/5";
                    $.rating.text = intr;
                    Ti.API.info("----------------fine query------------");
                }
            }
        });
    };
    __defers["$.__views.addbook!click!addFun"] && $.addListener($.__views.addbook, "click", addFun);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;