function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "notifView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.noitf = Ti.UI.createView({
        top: 0,
        right: 0,
        width: "50%",
        height: "90%",
        backgroundColor: "gray",
        opacity: .3,
        zIndex: 1e4,
        id: "noitf",
        visible: false
    });
    $.__views.noitf && $.addTopLevelView($.__views.noitf);
    $.__views.d = Ti.UI.createLabel({
        color: "black",
        opacity: .5,
        text: "è arrivata una notifica",
        id: "d",
        top: 1
    });
    $.__views.noitf.add($.__views.d);
    $.__views.scrollNotif = Ti.UI.createTableView({
        id: "scrollNotif"
    });
    $.__views.noitf.add($.__views.scrollNotif);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var data = [];
    exports.addElm = function(row) {
        Ti.API.info("Agguingo la notificata alla lista");
        Ti.API.info("la row aldilà dello specchio->" + row);
        data.push(row);
        $.scrollNotif.data = data;
        Ti.API.info("fine addata");
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;