function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function fill() {
        Ti.API.info("Parto retrive da profview");
        var rel = Parse.User.current().relation("myBooks");
        var query = rel.query();
        query.include("book");
        query.find().then(function(results) {
            if (0 == results.length) Ti.API.info("non ci sono libri"); else for (var i = 0; i < results.length; i++) if (true == results[i].get("read") && false == results[i].get("favorite")) {
                $.libScroll.remove($.l1);
                Ti.API.info("ciclo");
                var idf = results[i].id;
                Ti.API.info("id->" + results[i].id);
                {
                    results[i].get("book").get("title");
                }
                Ti.API.info("titolo->" + results[i].get("book").get("title"));
                {
                    results[i].get("book").get("authors");
                }
                Ti.API.info("aut->" + results[i].get("book").get("authors"));
                var img = results[i].get("book").get("imageName");
                Ti.API.info("img->" + results[i].get("book").get("imageName"));
                {
                    results[i].get("read");
                }
                Ti.API.info("read->" + results[i].get("read"));
                {
                    results[i].get("favorite");
                }
                Ti.API.info("fav->" + results[i].get("favorite"));
                var isbn10 = results[i].get("book").get("isbn");
                Ti.API.info("Creo immagine");
                Alloy.Globals.libri.push(isbn10);
                var im1 = Ti.UI.createImageView({
                    image: img,
                    width: "25%",
                    height: "260px",
                    left: 25,
                    bottom: 5,
                    top: 10
                });
                im1.idM = idf;
                im1.addEventListener("longclick", function(e) {
                    var alt = Ti.UI.createAlertDialog({
                        cancel: 1,
                        buttonNames: [ "No", "Si" ],
                        backgroundColor: "white",
                        message: "Vuoi aggiungere questo libro ai preferiti ?",
                        title: "Sei Sicuro"
                    });
                    alt.imgSource = e.source;
                    alt.addEventListener("click", function(ex) {
                        if (ex.index === ex.source.cancel) {
                            var parseC = require("parseCall");
                            parseC.setFav(ex.source.imgSource.idM);
                            $.vImg.add(ex.source.imgSource);
                        }
                    });
                    alt.show();
                });
                Ti.API.info("Addo libro a libreria");
                $.libScroll.add(im1);
            } else if (true == results[i].get("read") && true == results[i].get("favorite")) {
                Ti.API.info("ciclo");
                $.pv.remove($.l);
                var idf = results[i].id;
                Ti.API.info("id->" + results[i].id);
                {
                    results[i].get("book").get("title");
                }
                Ti.API.info("titolo->" + results[i].get("book").get("title"));
                {
                    results[i].get("book").get("authors");
                }
                Ti.API.info("aut->" + results[i].get("book").get("authors"));
                var img = results[i].get("book").get("imageName");
                Ti.API.info("img->" + results[i].get("book").get("imageName"));
                {
                    results[i].get("read");
                }
                Ti.API.info("read->" + results[i].get("read"));
                {
                    results[i].get("favorite");
                }
                Ti.API.info("fav->" + results[i].get("favorite"));
                var is10 = results[i].get("book").get("isbn");
                Alloy.Globals.libri.push(is10);
                Ti.API.info("Creo immagine");
                var im1 = Ti.UI.createImageView({
                    image: img,
                    width: "25%",
                    height: "260px",
                    left: 25,
                    top: 10,
                    bottom: 5
                });
                im1.idM = idf;
                $.vImg.add(im1);
            }
        });
    }
    function openFwer() {
        Alloy.createController("follower").getView().open();
    }
    function openFwing() {
        Alloy.createController("following").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "profView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.dd = Ti.UI.createWindow({
        id: "dd"
    });
    $.__views.dd && $.addTopLevelView($.__views.dd);
    $.__views.profId = Ti.UI.createScrollView({
        layout: "vertical",
        backgroundColor: "white",
        backgroundImage: "/images/wood.jpg",
        id: "profId"
    });
    $.__views.dd.add($.__views.profId);
    $.__views.__alloyId37 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId37"
    });
    $.__views.profId.add($.__views.__alloyId37);
    $.__views.banner = Ti.UI.createView({
        height: "200px",
        id: "banner",
        top: 0
    });
    $.__views.__alloyId37.add($.__views.banner);
    $.__views.__alloyId38 = Ti.UI.createView({
        height: "250px",
        backgroundColor: "#296593",
        top: "200px",
        bottom: 0,
        id: "__alloyId38"
    });
    $.__views.__alloyId37.add($.__views.__alloyId38);
    $.__views.nome = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "black",
        top: 15,
        id: "nome"
    });
    $.__views.__alloyId38.add($.__views.nome);
    $.__views.citazi = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "black",
        bottom: 15,
        id: "citazi"
    });
    $.__views.__alloyId38.add($.__views.citazi);
    $.__views.prof = Ti.UI.createImageView({
        top: "100px",
        left: 15,
        height: "200px",
        width: "200px",
        borderRadius: 40,
        id: "prof",
        image: "/images/prof-default.png"
    });
    $.__views.__alloyId37.add($.__views.prof);
    $.__views.__alloyId39 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId39"
    });
    $.__views.profId.add($.__views.__alloyId39);
    $.__views.bfer = Ti.UI.createButton({
        color: "#224d72",
        borderColor: "#224d72",
        backgroundColor: "white",
        title: "Follower",
        id: "bfer",
        left: 0,
        width: "50%"
    });
    $.__views.__alloyId39.add($.__views.bfer);
    openFwer ? $.addListener($.__views.bfer, "click", openFwer) : __defers["$.__views.bfer!click!openFwer"] = true;
    $.__views.bfer = Ti.UI.createButton({
        color: "#224d72",
        borderColor: "#224d72",
        backgroundColor: "white",
        title: "Following",
        id: "bfer",
        right: 0,
        width: "50%"
    });
    $.__views.__alloyId39.add($.__views.bfer);
    openFwing ? $.addListener($.__views.bfer, "click", openFwing) : __defers["$.__views.bfer!click!openFwing"] = true;
    $.__views.prefer = Ti.UI.createView({
        height: 25,
        backgroundColor: "#f2f2f2",
        id: "prefer"
    });
    $.__views.profId.add($.__views.prefer);
    $.__views.preferiti = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        text: "Preferiti",
        id: "preferiti"
    });
    $.__views.prefer.add($.__views.preferiti);
    $.__views.pv = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "pv",
        backgroundColor: "white"
    });
    $.__views.profId.add($.__views.pv);
    $.__views.l = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "black",
        text: "Non hai libri preferiti...Leggi un libro e tieni premuta la copertina per aggiungerlo nei preferiti!",
        id: "l",
        left: "40px",
        right: "40px",
        top: "40px",
        bottom: "40px"
    });
    $.__views.pv.add($.__views.l);
    $.__views.vImg = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "vImg",
        layout: "horizontal"
    });
    $.__views.pv.add($.__views.vImg);
    $.__views.prefer = Ti.UI.createView({
        height: 25,
        backgroundColor: "#f2f2f2",
        id: "prefer"
    });
    $.__views.profId.add($.__views.prefer);
    $.__views.preferiti = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        text: "Libri Letti",
        id: "preferiti"
    });
    $.__views.prefer.add($.__views.preferiti);
    $.__views.__alloyId40 = Ti.UI.createView({
        height: "36px",
        backgroundImage: "/images/soprawood.png",
        id: "__alloyId40"
    });
    $.__views.profId.add($.__views.__alloyId40);
    $.__views.libScroll = Ti.UI.createView({
        height: Ti.UI.SIZE,
        backgroundImage: "/images/wood.jpg",
        id: "libScroll",
        layout: "horizontal"
    });
    $.__views.profId.add($.__views.libScroll);
    $.__views.l1 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "white",
        text: "Non hai letto nessun libro...Usa il tasto cerca e buona lettura!",
        id: "l1",
        left: "40px",
        right: "40px",
        top: "40px",
        bottom: "40px"
    });
    $.__views.libScroll.add($.__views.l1);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var usr = Parse.User.current();
    var c = require("parseCall");
    if (usr) {
        fill();
        var nick = usr.get("username");
        var cit = usr.get("cit");
        $.nome.setText(nick);
        var s = usr.get("banner");
        var b = c.banner(s);
        $.banner.setBackgroundImage(b);
        $.citazi.setText("'' " + cit + " ''");
    }
    $.nome.setText(nick);
    $.citazi.setText("'' " + cit + " ''");
    var img = Ti.App.Properties.getString("imagepath");
    Ti.API.info("imagepath->" + img);
    $.prof.setImage(img);
    __defers["$.__views.bfer!click!openFwer"] && $.addListener($.__views.bfer, "click", openFwer);
    __defers["$.__views.bfer!click!openFwing"] && $.addListener($.__views.bfer, "click", openFwing);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;