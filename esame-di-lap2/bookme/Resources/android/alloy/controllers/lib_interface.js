function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "lib_interface";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.lib_interface = Ti.UI.createWindow({
        backgroundColor: "black",
        id: "lib_interface"
    });
    $.__views.lib_interface && $.addTopLevelView($.__views.lib_interface);
    var __alloyId33 = [];
    $.__views.row = Ti.UI.createTableViewRow({
        id: "row"
    });
    __alloyId33.push($.__views.row);
    $.__views.single_row = Ti.UI.createView({
        id: "single_row"
    });
    $.__views.row.add($.__views.single_row);
    $.__views.cover = Ti.UI.createImageView({
        left: 5,
        id: "cover"
    });
    $.__views.single_row.add($.__views.cover);
    libView ? $.addListener($.__views.cover, "click", libView) : __defers["$.__views.cover!click!libView"] = true;
    $.__views.name_lib = Ti.UI.createLabel({
        top: 5,
        id: "name_lib"
    });
    $.__views.single_row.add($.__views.name_lib);
    $.__views.author_lib = Ti.UI.createLabel({
        bottom: 5,
        id: "author_lib"
    });
    $.__views.single_row.add($.__views.author_lib);
    $.__views.btn = Ti.UI.createImageView({
        right: 5,
        id: "btn",
        src: "/btn_file.jpg"
    });
    $.__views.single_row.add($.__views.btn);
    completeLib ? $.addListener($.__views.btn, "click", completeLib) : __defers["$.__views.btn!click!completeLib"] = true;
    $.__views.list_tab = Ti.UI.createTableView({
        data: __alloyId33,
        id: "list_tab"
    });
    $.__views.lib_interface.add($.__views.list_tab);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.cover!click!libView"] && $.addListener($.__views.cover, "click", libView);
    __defers["$.__views.btn!click!completeLib"] && $.addListener($.__views.btn, "click", completeLib);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;