function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function bye() {
        pRequest.setRead(idMy);
        pRequest.addratingMy(idMy, valuta + 1);
        var req = {
            book: iltitolo
        };
        Ti.API.info("---------------iltitolo->" + iltitolo + "---------");
        Parse.Cloud.run("ratingAverange", req);
        Ti.App.fireEvent("raimondo");
        Alloy.createController("index").getView().open();
        $.eee.hide();
    }
    function submit() {
        var comment = Parse.Object.extend("commentModel");
        var actCom = new comment();
        var usr = Parse.User.current().getUsername();
        var strCm = $.area.value;
        actCom.save({
            user: usr,
            book: iltitolo,
            commenttext: strCm,
            myRating: valuta + 1,
            like: 0
        }, {
            success: function() {
                alert("Commentato");
            },
            error: function() {
                Ti.API.info("Comment no riuscito");
            }
        });
        pRequest.setRead(idMy);
        pRequest.addratingMy(idMy, valuta + 1);
        var req = {
            book: iltitolo
        };
        Parse.Cloud.run("ratingAverange", req);
        Ti.App.fireEvent("raimondo");
        Alloy.createController("index").getView().open();
        $.eee.hide();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "leavecomment";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.leavecomm = Ti.UI.createView({
        backgroundColor: "white",
        id: "leavecomm",
        layout: "vertical"
    });
    $.__views.__alloyId25 = Ti.UI.createView({
        id: "__alloyId25"
    });
    $.__views.leavecomm.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createLabel({
        text: "Dai una valutazione al libro",
        color: "black",
        id: "__alloyId26"
    });
    $.__views.__alloyId25.add($.__views.__alloyId26);
    $.__views.rating = Ti.UI.createView({
        id: "rating",
        layout: "horizontal",
        left: "72px"
    });
    $.__views.leavecomm.add($.__views.rating);
    $.__views.__alloyId27 = Ti.UI.createView({
        top: "20px",
        id: "__alloyId27"
    });
    $.__views.leavecomm.add($.__views.__alloyId27);
    $.__views.area = Ti.UI.createTextArea({
        borderColor: "#224d72",
        id: "area",
        hintText: "Scrivi cosa ne pensi...",
        hintTextColor: "gray",
        color: "black",
        height: "400px",
        width: "400px"
    });
    $.__views.__alloyId27.add($.__views.area);
    $.__views.__alloyId28 = Ti.UI.createView({
        top: "10px",
        id: "__alloyId28"
    });
    $.__views.leavecomm.add($.__views.__alloyId28);
    $.__views.__alloyId29 = Ti.UI.createLabel({
        backgroundColor: "#224d72",
        width: "94%",
        height: 2,
        id: "__alloyId29"
    });
    $.__views.__alloyId28.add($.__views.__alloyId29);
    $.__views.__alloyId30 = Ti.UI.createView({
        top: "10px",
        id: "__alloyId30"
    });
    $.__views.leavecomm.add($.__views.__alloyId30);
    $.__views.__alloyId31 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        width: "40%",
        color: "white",
        title: "No,grazie",
        left: 0,
        id: "__alloyId31"
    });
    $.__views.__alloyId30.add($.__views.__alloyId31);
    bye ? $.addListener($.__views.__alloyId31, "click", bye) : __defers["$.__views.__alloyId31!click!bye"] = true;
    $.__views.__alloyId32 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        width: "40%",
        color: "white",
        title: "Commenta",
        right: 0,
        id: "__alloyId32"
    });
    $.__views.__alloyId30.add($.__views.__alloyId32);
    submit ? $.addListener($.__views.__alloyId32, "click", submit) : __defers["$.__views.__alloyId32!click!submit"] = true;
    $.__views.eee = Ti.UI.createAlertDialog({
        height: "90%",
        androidView: $.__views.leavecomm,
        id: "eee"
    });
    $.__views.eee && $.addTopLevelView($.__views.eee);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var iltitolo;
    var idMy;
    var pRequest = require("parseCall");
    this.gettitolo = function(e) {
        iltitolo = e;
    };
    this.getidMy = function(e) {
        Ti.API.info("e è->" + e);
        idMy = e;
    };
    var star = [ 5 ];
    var valuta;
    for (var i = 0; 5 > i; i++) {
        var starimage = Ti.UI.createImageView({
            image: "/images/starempty.png",
            left: "40px",
            top: "20px",
            height: "45px",
            width: "45px"
        });
        starimage.idstar = i;
        star[i] = starimage;
        starimage.addEventListener("click", function(e) {
            Ti.API.info("id stella: " + e.source.idstar);
            for (var j = e.source.idstar; j >= 0; j--) {
                star[j].image = "/images/starfull.png";
                valuta = e.source.idstar;
            }
            for (var k = e.source.idstar + 1; 5 > k; k++) star[k].image = "/images/starempty.png";
        });
        $.rating.add(star[i]);
    }
    __defers["$.__views.__alloyId31!click!bye"] && $.addListener($.__views.__alloyId31, "click", bye);
    __defers["$.__views.__alloyId32!click!submit"] && $.addListener($.__views.__alloyId32, "click", submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;