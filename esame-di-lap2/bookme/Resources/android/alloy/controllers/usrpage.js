function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function segui() {
        $.b1.title;
        if (0 == followerCheck) {
            Ti.API.info("----------------Chi sei?---------------");
            Ti.API.info("Nome->" + utenteclicccato.get("username"));
            Ti.API.info("----------------------------------------");
            var ut1s = Parse.User.current().get("username");
            var ut2s = utenteclicccato.get("username");
            var vetta = [ ut1s, ut2s ];
            var req = {
                vett: vetta
            };
            Parse.Cloud.run("addFollow", req);
            $.b1.setTitle("Non Seguire");
        } else {
            Ti.API.info("------------eliminazione follower-------------------");
            var ut1s = Parse.User.current().get("username");
            var ut2s = utenteclicccato.get("username");
            var vetta = [ ut1s, ut2s ];
            var req = {
                vett: vetta
            };
            Parse.Cloud.run("remFollow", req);
            $.b1.setTitle("Segui");
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "usrpage";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.b = Ti.UI.createWindow({
        layout: "vertical",
        backgroundColor: "white",
        backgroundImage: "/images/wood.jpg",
        id: "b"
    });
    $.__views.b && $.addTopLevelView($.__views.b);
    $.__views.__alloyId71 = Ti.UI.createScrollView({
        layout: "vertical",
        id: "__alloyId71"
    });
    $.__views.b.add($.__views.__alloyId71);
    $.__views.banner = Ti.UI.createView({
        height: "200px",
        id: "banner"
    });
    $.__views.__alloyId71.add($.__views.banner);
    $.__views.b1 = Ti.UI.createButton({
        borderRadius: 10,
        borderWidth: 2,
        borderColor: "#224d72",
        backgroundColor: "white",
        color: "#224d72",
        top: 10,
        left: 10,
        id: "b1",
        title: "segui"
    });
    $.__views.banner.add($.__views.b1);
    segui ? $.addListener($.__views.b1, "click", segui) : __defers["$.__views.b1!click!segui"] = true;
    $.__views.__alloyId72 = Ti.UI.createView({
        height: "250px",
        backgroundColor: "#296593",
        id: "__alloyId72"
    });
    $.__views.__alloyId71.add($.__views.__alloyId72);
    $.__views.l1 = Ti.UI.createLabel({
        color: "black",
        text: "nome",
        id: "l1",
        top: 3
    });
    $.__views.__alloyId72.add($.__views.l1);
    $.__views.l2 = Ti.UI.createLabel({
        color: "black",
        text: "citazione",
        id: "l2"
    });
    $.__views.__alloyId72.add($.__views.l2);
    $.__views.prefer = Ti.UI.createView({
        height: 25,
        backgroundColor: "#f2f2f2",
        id: "prefer"
    });
    $.__views.__alloyId71.add($.__views.prefer);
    $.__views.l3 = Ti.UI.createLabel({
        color: "black",
        text: "preferiti",
        id: "l3"
    });
    $.__views.prefer.add($.__views.l3);
    $.__views.pv = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "pv",
        backgroundColor: "white"
    });
    $.__views.__alloyId71.add($.__views.pv);
    $.__views.l = Ti.UI.createLabel({
        color: "black",
        text: "Non ha nesssun libro preferito!",
        id: "l",
        left: "40px",
        right: "40px",
        top: "40px",
        bottom: "40px"
    });
    $.__views.pv.add($.__views.l);
    $.__views.vImg = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "vImg",
        layout: "horizontal"
    });
    $.__views.pv.add($.__views.vImg);
    $.__views.prefer = Ti.UI.createView({
        height: 25,
        backgroundColor: "#f2f2f2",
        id: "prefer"
    });
    $.__views.__alloyId71.add($.__views.prefer);
    $.__views.preferiti = Ti.UI.createLabel({
        color: "black",
        text: "Libri Letti",
        id: "preferiti"
    });
    $.__views.prefer.add($.__views.preferiti);
    $.__views.__alloyId73 = Ti.UI.createView({
        height: "36px",
        backgroundImage: "/images/soprawood.png",
        id: "__alloyId73"
    });
    $.__views.__alloyId71.add($.__views.__alloyId73);
    $.__views.libScroll = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "libScroll",
        backgroundImage: "/images/wood.jpg",
        layout: "horizontal"
    });
    $.__views.__alloyId71.add($.__views.libScroll);
    $.__views.notL = Ti.UI.createLabel({
        color: "white",
        text: "Non ha letto nessun libro!",
        id: "notL",
        left: "40px",
        right: "40px",
        top: "40px",
        bottom: "40px"
    });
    $.__views.libScroll.add($.__views.notL);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var ban = require("parseCall");
    var uteneio = Parse.User.current();
    var utenteclicccato;
    var followerCheck = 0;
    this.dammiiltitolo = function(e) {
        $.b.setTitle(e);
        var users = e;
        $.l1.setText(users);
        var rel = uteneio.relation("following");
        var q = rel.query();
        q.equalTo("username", users);
        q.find({
            success: function(results) {
                var check = results[0].get("username");
                Ti.API.info("---------------cucù chi è?----------------");
                Ti.API.info("--------" + check + "--------------------------");
                if (0 == check.localeCompare(users)) {
                    Ti.API.info("------ho trovato chi mi segue-----------");
                    followerCheck = 1;
                    $.b1.setTitle("Non Seguire");
                } else Ti.API.info("----------non mi segue----------");
            },
            error: function() {
                Ti.API.info("----------------------------FALITTO---------------------------------");
            }
        });
        var query = new Parse.Query(Parse.User);
        query.equalTo("username", users);
        query.find({
            success: function(results) {
                utenteclicccato = results[0];
                var x = results[0].get("cit");
                $.l2.setText("'' " + x + " ''");
                var bnn = results[0].get("banner");
                var d = ban.banner(bnn);
                $.banner.backgroundImage = d;
                var rel = results[0].relation("myBooks");
                var q = rel.query();
                q.include("book");
                q.find().then(function(risultato) {
                    for (var i = 0; i < risultato.length; i++) if (0 == risultato.length) Ti.API.info("nessun libro letto"); else {
                        if (true == risultato[i].get("read") && false == risultato[i].get("favorite")) {
                            $.libScroll.remove($.notL);
                            var img = risultato[i].get("book").get("imageName");
                            var im1 = Ti.UI.createImageView({
                                image: img,
                                width: "25%",
                                height: "260px",
                                left: 25,
                                bottom: 5,
                                top: 10
                            });
                            $.libScroll.add(im1);
                        }
                        if (true == risultato[i].get("read") && true == risultato[i].get("favorite")) {
                            $.pv.remove($.l);
                            var img = risultato[i].get("book").get("imageName");
                            var im1 = Ti.UI.createImageView({
                                image: img,
                                width: "25%",
                                height: "260px",
                                left: 25,
                                bottom: 5,
                                top: 10
                            });
                            $.vImg.add(im1);
                        }
                    }
                });
            },
            error: function() {}
        });
    };
    __defers["$.__views.b1!click!segui"] && $.addListener($.__views.b1, "click", segui);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;