function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function logtipoout() {
        var currentUser = Parse.User.current();
        if (currentUser) {
            alert("Arrivederci :-)");
            Parse.User.logOut();
            Alloy.createController("logView").getView().open();
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "impView";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.impView = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "impView"
    });
    $.__views.impView && $.addTopLevelView($.__views.impView);
    $.__views.logout = Ti.UI.createButton({
        borderRadius: 40,
        backgroundColor: "#224d72",
        title: "LogOut",
        id: "logout"
    });
    $.__views.impView.add($.__views.logout);
    logtipoout ? $.addListener($.__views.logout, "click", logtipoout) : __defers["$.__views.logout!click!logtipoout"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    __defers["$.__views.logout!click!logtipoout"] && $.addListener($.__views.logout, "click", logtipoout);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;