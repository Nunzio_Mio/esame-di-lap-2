function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function retriveCommLetti() {
        Ti.API.info("ENTRO");
        var lettiLibri = [];
        var rel = Parse.User.current().relation("myBooks");
        var query = rel.query();
        query.include("book");
        query.find().then(function(results) {
            if (0 == results.length) alert("Non c'è nessun problema"); else for (var i = 0; i < results.length; i++) {
                Ti.API.info("SONO NEL FOR");
                if (true == results[i].get("read")) {
                    var nomLibro = results[i].get("book").get("title");
                    Ti.API.info("NOME LIBRO " + nomLibro);
                    lettiLibri[i] = nomLibro;
                    Ti.API.info("-------------------");
                    Ti.API.info("elemento: " + lettiLibri[i]);
                    Ti.API.info("---------------------");
                }
            }
            Ti.API.info("-_-_-_-_-_-seconda parte della query");
            var commentParse = Parse.Object.extend("commentModel");
            var q = new Parse.Query(commentParse);
            q.descending("createdAt");
            q.find().then(function(results) {
                if (0 == results.length) ; else for (var i = 0; i < results.length; i++) for (var j = 0; j < lettiLibri.length; j++) if (lettiLibri[j] == results[i].get("book")) {
                    {
                        results[i].id;
                    }
                    var book = results[i].get("book");
                    var commText = results[i].get("commenttext");
                    var user = results[i].get("user");
                    var like = results[i].get("like");
                    var comment = {
                        user: user,
                        book: book,
                        commenttext: commText,
                        like: like
                    };
                    var Crow = Alloy.createController("simpRow", comment).getView();
                    dataB.push(Crow);
                    $.table.setData(dataB);
                }
            });
        });
    }
    function retriveCommFoll() {
        var arruser = [];
        var rel = Parse.User.current().relation("following");
        var query = rel.query();
        query.find().then(function(results) {
            if (0 == results.length) {
                var label = Ti.UI.createLabel({
                    color: "black",
                    text: "Non hai following...Usa il tasto cerca per trovare chi seguire",
                    top: "300px",
                    bottom: "300px",
                    left: "300px",
                    right: "300px"
                });
                $v2.add(label);
            } else for (var i = 0; i < results.length; i++) {
                var nuser = results[i].get("username");
                arruser[i] = nuser;
            }
            var commentParse = Parse.Object.extend("commentModel");
            var q = new Parse.Query(commentParse);
            q.descending("createdAt");
            q.containedIn("user", arruser);
            q.find().then(function(results) {
                if (0 == results.length) ; else for (var i = 0; i < results.length; i++) {
                    {
                        results[i].id;
                    }
                    var book = results[i].get("book");
                    var commText = results[i].get("commenttext");
                    var user = results[i].get("user");
                    var like = results[i].get("like");
                    var comment = {
                        user: user,
                        book: book,
                        commenttext: commText,
                        like: like
                    };
                    var Crow = Alloy.createController("simpRow", comment).getView();
                    dataC.push(Crow);
                    $.table2.setData(dataC);
                }
            });
        });
    }
    function follo() {
        $.v1.height = 0;
        $.v1.hide();
        $.v2.height = Ti.UI.FILL;
        $.v2.show();
        retriveCommFoll();
    }
    function Comm() {
        $.v1.height = Ti.UI.FILL;
        $.v1.show();
        $.v2.height = 0;
        $.v2.hide();
        retriveCommLetti();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "simposio";
    this.args = arguments[0] || {};
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.simp = Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        id: "simp"
    });
    $.__views.simp && $.addTopLevelView($.__views.simp);
    $.__views.__alloyId62 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId62"
    });
    $.__views.simp.add($.__views.__alloyId62);
    $.__views.__alloyId63 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        color: "white",
        title: "Following",
        right: 0,
        top: 0,
        width: "50%",
        id: "__alloyId63"
    });
    $.__views.__alloyId62.add($.__views.__alloyId63);
    follo ? $.addListener($.__views.__alloyId63, "click", follo) : __defers["$.__views.__alloyId63!click!follo"] = true;
    $.__views.__alloyId64 = Ti.UI.createButton({
        backgroundColor: "#224d72",
        color: "white",
        title: "Commenti",
        top: 0,
        width: "50%",
        left: 0,
        id: "__alloyId64"
    });
    $.__views.__alloyId62.add($.__views.__alloyId64);
    Comm ? $.addListener($.__views.__alloyId64, "click", Comm) : __defers["$.__views.__alloyId64!click!Comm"] = true;
    $.__views.v1 = Ti.UI.createView({
        id: "v1"
    });
    $.__views.simp.add($.__views.v1);
    $.__views.table = Ti.UI.createTableView({
        id: "table"
    });
    $.__views.v1.add($.__views.table);
    $.__views.v2 = Ti.UI.createView({
        id: "v2"
    });
    $.__views.simp.add($.__views.v2);
    $.__views.table2 = Ti.UI.createTableView({
        id: "table2"
    });
    $.__views.v2.add($.__views.table2);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.args;
    var dataB = [];
    var dataC = [];
    $.v1.show();
    $.v2.hide();
    Parse.User.current() && retriveCommLetti();
    __defers["$.__views.__alloyId63!click!follo"] && $.addListener($.__views.__alloyId63, "click", follo);
    __defers["$.__views.__alloyId64!click!Comm"] && $.addListener($.__views.__alloyId64, "click", Comm);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;