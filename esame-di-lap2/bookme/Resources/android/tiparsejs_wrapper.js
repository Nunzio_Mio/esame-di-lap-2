var TiParse = function(options) {
    Parse = require("TiParseJS");
    Parse.saveEventually = {
        localStorageKey: "Parse.saveEventually.Queue",
        initialize: function() {},
        save: function(objectType, object) {
            this.addToQueue("save", objectType, object);
        },
        addToQueue: function(action, objectType, object) {
            var queueData = this.getQueue();
            var queueId = [ objectType, object.id, object.get("hash") ].join("_");
            var i = this.queueItemExists(queueData, queueId);
            if (i > -1) for (var prop in queueData[i].data) "undefined" == object.get(prop) && object.set(prop, queueData[i].data[prop]); else i = queueData.length;
            queueData[i] = {
                queueId: queueId,
                type: objectType,
                action: action,
                id: object.id,
                hash: object.get("hash"),
                createdAt: new Date(),
                data: object
            };
            this.setQueue(queueData);
        },
        getQueue: function() {
            var q = Parse.localStorage.getItem(this.localStorageKey) || null;
            return q && "object" == typeof JSON.parse(q) ? JSON.parse(q) : [];
        },
        setQueue: function(queueData) {
            Parse.localStorage.setItem(this.localStorageKey, JSON.stringify(queueData));
        },
        clearQueue: function() {
            Parse.localStorage.setItem(this.localStorageKey, JSON.stringify([]));
        },
        queueItemExists: function(queueData, queueId) {
            for (var i = 0; i < queueData.length; i++) if (queueData[i].queueId == queueId) return i;
            return -1;
        },
        countQueue: function() {
            return this.getQueue().length;
        },
        sendQueue: function() {
            var queueData = this.getQueue();
            if (queueData.length < 1) return false;
            for (var i = 0; i < queueData.length; i++) {
                var myObjectType = Parse.Object.extend(queueData[i].type);
                queueData[i].id ? this.reprocess.byId(myObjectType, queueData[i]) : queueData[i].hash ? this.reprocess.byHash(myObjectType, queueData[i]) : this.reprocess.create(myObjectType, queueData[i]);
            }
            return true;
        },
        sendQueueCallback: function(myObject, queueObject) {
            switch (queueObject.action) {
              case "save":
                if ("undefined" != typeof myObject.updatedAt && myObject.updatedAt > new Date(queueObject.createdAt)) return false;
                myObject.save(queueObject.data, {
                    success: function(object) {
                        Ti.API.debug(object);
                    },
                    error: function(model, error) {
                        Ti.API.error(error);
                    }
                });
                break;

              case "delete":            }
        },
        reprocess: {
            create: function(myObjectType, queueObject) {
                var myObject = new myObjectType();
                Parse.saveEventually.sendQueueCallback(myObject, queueObject);
            },
            byId: function(myObjectType, queueObject) {
                var query = new Parse.Query(myObjectType);
                query.get(queueObject.id, {
                    success: function(myObject) {
                        Parse.saveEventually.sendQueueCallback(myObject, queueObject);
                    },
                    error: function() {}
                });
            },
            byHash: function(myObjectType, queueObject) {
                var query = new Parse.Query(myObjectType);
                query.equalTo("hash", queueObject.hash);
                query.find({
                    success: function(results) {
                        results.length > 0 ? Parse.saveEventually.sendQueueCallback(results[0], queueObject) : Parse.saveEventually.reprocess.create(myObjectType, queueObject);
                    },
                    error: function() {}
                });
            }
        }
    };
    Parse.initialize(options.applicationId, options.javascriptkey);
    Parse.serverURL = "https://bookmef-todol.rhcloud.com/parse";
};

module.exports = TiParse;