exports.CreateDB = function() {
    var db = Ti.Database.open("Check");
    db.execute("CREATE TABLE IF NOT EXISTS libri(id PRIMARY KEY, isbn10 INTEGER)");
    return db;
};

exports.InsertDB = function(db, isbn10) {
    db.execute("INSERT INTO libri(isbn10)VALUES(?)", isbn10);
    db.close();
};

exports.loadDB = function(db) {
    var res = db.execute("SELECT * FROM libri");
    var rowArray = [];
    for (i = 0, j = res.rowCount; j > i; i++) {
        rowArray.push(res.fieldByName("isbn10"));
        res.next();
    }
    return rowArray;
};